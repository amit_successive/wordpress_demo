<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package modern_art_successive
 */
get_header();
?>
<div id="primary" class="content-area">
    <main id="main" class="site-main">
        <section class="error-404 not-found">
            <header class="page-header">
                <img src="<?php echo TEMPLATE_URL;?>/assets/images/error.jpg" class="img-responsive" alt="404 Image">
                <h1 class="page-title"><?php esc_html_e('Oops! somthing went wrong!', 'modern-art-successive');?></h1>
            </header>
            <!-- .page-header -->
            <div class="page-content">
                <a href="<?php echo site_url(); ?>" class="btn custom-btn1"> Go to home page</a>
            </div>
            <!-- .page-content -->
        </section>
        <!-- .error-404 -->
    </main>
    <!-- #main -->
</div>
<!-- #primary -->
<?php
get_footer();
