<!-- sidebar btns -->
 <?php 
 ?>
    <!-- latest news section-->

    <div class="col-xl-6 col-lg-6 col-md-8 padding-delete">
        <div class="mid-section">
            <div class="row">
                <div class="mid-section-inner-wrapper">
                    <h3 class="hwt-artz">latest news</h3>
                    <hr>
                    <div class="row news-page-content-wrapper">
                        <div class="col-md-6 news-page-left">
                            <h3 class="sub-title">
                                <?php echo get_the_title();?>
                            </h3>

                            <?php
                                // TO SHOW THE PAGE CONTENTS
                                while (have_posts()) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
                                    <div class="entry-content-page">
                                        <?php the_content(); ?> <!-- Page Content -->
                                    </div><!-- .entry-content-page -->
                                <?php
                                endwhile;
                                wp_reset_query();
                                ?>
                           
                        </div>

                        <div class="col-md-6 news-page-right">
                            <?php
                                if ( has_post_thumbnail() ) { ?>
                                    <img src="<?php echo get_feature_image_url(get_the_ID(), 'full' );?>" class="img-responsive" alt="Image">
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- app advertisement -->
<?php ?>
<!-- app advertisement -->