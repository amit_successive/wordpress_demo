<?php 
    /* Template Name: Logout */
    if (!is_user_logged_in()) { wp_redirect(home_url('login')); exit; }
    get_header();
    
    ?>
<!-- sidebar btns -->
<?php include_once('common/left_side_menu.php') ?>
<!-- latest news section-->
<!-- Logout form -->
<div class="col-xl-6 col-lg-6 col-md-8 padding-delete">
    <div class="mid-section">
        <div class="row">
            <div class="mid-section-inner-wrapper">
                <h3 class="div-title-2">Yes, log me out</h3>
                <hr>
                <p>
                    You can always log back in.
                </p>
                <a href="<?php echo wp_logout_url(); ?>" class="btn custom-btn1" type="button">Log out</a>
            </div>
        </div>
    </div>
</div>
<!-- Logout form -->
<!-- app advertisement -->
<?php include_once('common/right_side_menu.php') ?>
<!-- app advertisement -->
<?php get_footer(); ?>