<?php 
    /* Template Name: Delete Profile */
    if (!is_user_logged_in()) { wp_redirect(home_url('login'));}
    if(isset($_REQUEST['delete']) && $_REQUEST['delete'] == 'true'){
        if(current_user_can('editor') || current_user_can('administrator')){
            $error = "Admin can't delete his account.";
        }else{
            require_once( ABSPATH.'wp-admin/includes/user.php' );
            wp_delete_user(get_current_user_id(), get_option("active_sfd_user"));
            wp_redirect(site_url('/dashboard/?delete=success'));
        }
    }
    get_header();
?>
<!-- sidebar btns -->
<?php include_once('common/left_side_menu.php') ?>
<!-- latest news section-->
<!-- Logout form -->
<div class="col-xl-6 col-lg-6 col-md-8 padding-delete">
    <div class="mid-section">
        <div class="row">
            <div class="mid-section-inner-wrapper">
                <h3 class="div-title-2">Delete my profile</h3>
                <hr>
                <p>
                    This will wipe out all your info. Come back any time you want and reregister.
                </p>
                <?php
                    if( current_user_can('editor') || current_user_can('administrator') ) { ?>
                        <p>You can't delete your Account because you have admin access.</p>
                    <?php }else{ ?>
                        <button class="btn custom-btn1" type="button" onClick="deleteUserConfirm('<?php echo site_url();?>'+'/delete-profile?delete=true');">Delete</button>
                <?php }
                ?>
            </div>
        </div>
    </div>
</div>
<!-- Logout form -->
<!-- app advertisement -->
<?php include_once('common/right_side_menu.php') ?>
<!-- app advertisement -->
<?php get_footer(); ?>