<?php 
    /* Template Name: HomePage */
    get_header();
    $excludePost = array();
    ?>
<!-- sidebar btns -->
<?php include_once('common/left_side_menu.php') ?>
<!-- latest news section-->
<div class="col-xl-6 col-lg-6 col-md-8 padding-delete">
    <div class="mid-section latest-news home_page">
        <h3 class="div-title">Latest News</h3>
        <hr>
        <div class="row">
            <div class="col-md-6 padding-delete">
                <div class="latest-news-right">
                    <?php 
                        $featurePostHome = get_field('feature_news');
                        array_push($excludePost, $featurePostHome->ID);
                    ?>
                        <a href="<?php echo get_permalink($featurePostHome->ID);?>">
                            <h3 class="sub-title"><?php echo $featurePostHome->post_title;?></h3>
                            <?php
                                if ( has_post_thumbnail($featurePostHome->ID) ) { ?>
                            <img src="<?php echo get_feature_image_url($featurePostHome->ID, 'thumbnail_size' );?>" class="img-responsive" alt="Image">
                            <?php } ?>
                            <p class="news-upper-text news-upper-text-no-justify"><?php echo $featurePostHome->post_excerpt;?></p>
                            <p class="news-text"><?php echo substr($featurePostHome->post_content,0,930);?></p>
                        </a>
                </div>
            </div>
            <div class="col-md-6 borderLeft">
                <div class="latest-news-left">
                    <?php
                        $feature3PostCount = 0;
                        $feature3PostHome = get_field('3_pinned_news');
                        foreach ($feature3PostHome as $single3Post) { 
                        array_push($excludePost, $single3Post->ID);
                            $feature3PostCount++;
                        ?>
                    
                        <h3 class="sub-div-title"><?php echo $single3Post->post_title;?></h3>
                        <p class="news-text"><?php echo ($single3Post->post_excerpt); ?><br>
                            <a class="read_more_left" href="<?php echo get_permalink($single3Post->ID);?>">Read more...</a>
                        </p>
                        
                        <?php if($feature3PostCount < count($feature3PostHome)){?>
                        <hr class="sidehr">
                        <?php } ?>
                    <?php }
                        ?>
                </div>
            </div>
            <hr>
            <div class="older-stories-wrapper">
                <div class="row">
                    <ul>
                        <?php 
                            $query = new WP_Query( array('post_type'=>'news', 'post__not_in' => $excludePost, 'order' => 'DESC',
                            'posts_per_page' => 4) );
                            if ( $query->have_posts() ) {
                            	while ( $query->have_posts() ) {
                            		$query->the_post(); ?>
                        <li>
                            <div class="older-stories">
                                
                                    <div class="row">
                                        <div class="older-stories-content col-md-10 col-sm-9">
                                            <h3 class="sub-div-title"><?php the_title();?></h3>
                                            <p class="news-text align-item-center">
                                            <?php echo substr(strip_tags(get_the_content()),0,250).'...'; ?>
                                            <a class="read_more" href="<?php echo get_permalink(get_the_ID());?>">Read more...</a>
                                            </p>
                                        </div>
                                        <div class="older-stories-img col-md-2 col-sm-3">
                                            <?php
                                                if ( has_post_thumbnail() ) { ?>
                                            <img src="<?php echo get_feature_image_url(get_the_ID(), 'thumbnail_size' );?>" class="img-responsive" alt="Image">
                                            <?php } ?>
                                        </div>
                                    </div>
                            </div>
                        </li>
                        <?php } // end while
                        wp_reset_query();
                            } // end if
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- app advertisement -->
<?php include_once('common/right_side_menu.php') ?>
<!-- app advertisement -->


<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-dialog-centered">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body mobilePopupbody">
        <img src="<?php echo TEMPLATE_URL; ?>/assets/images/hand-app.png" class="img-responsive popupHandimg" alt="hand-pop">
        <h4>Get the Free App!</h4>
        <p> Download the free app for everything on the website-plus handy tools to help manage and track your treatment.</p>
        <a href="http://onelink.to/gn7fn2" class="btn custom-btn1">Get the free app now</a>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
    </div>

  </div>
</div>
<?php get_footer(); ?>