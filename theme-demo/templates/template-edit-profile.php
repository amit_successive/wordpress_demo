<?php 
    /* Template Name: Edit Profile */
       if (!is_user_logged_in()) {
    	wp_redirect(home_url('login'));
    }
       /*
       * Submitting form data - Start
    */
    $utils = new Utils();
    $current_user = wp_get_current_user();
    $error = array();
    $selected = 'selected';
    $birth_date = 'birth_date';
    $gender = 'gender';
    $genderIdentity = 'gender_identity';
    $hiv_found_date = 'hiv_found_date';
    $art_start_date = 'art_start_date';
    $provinceILive = 'province_i_live';
    $cityILive = 'city_i_live';
    $institutionType = 'institution_type';
    $institutionName = 'institution_name';
    $onMedicalScheme = 'on_medical_scheme';
    $userEmail = $current_user->user_email;
    if(isset($_POST['username'])){
    	$submitedMail = sanitize_text_field($_POST['email']);
        $password = sanitize_text_field($_POST['password']);
    	(isset($_POST[$birth_date]))?update_user_meta($current_user->ID, $birth_date, date(DATE_NUMERIC, strtotime(sanitize_text_field($_POST[$birth_date])))): '';
    	(isset($_POST[$gender]))?update_user_meta($current_user->ID, $gender, sanitize_text_field($_POST[$gender])): '';
    	(isset($_POST[$genderIdentity]))?update_user_meta($current_user->ID, $genderIdentity, sanitize_text_field($_POST[$genderIdentity])): '';
    	(isset($_POST[$hiv_found_date]))?update_user_meta($current_user->ID, $hiv_found_date, date(DATE_NUMERIC, strtotime(sanitize_text_field($_POST[$hiv_found_date])))): '';
    	(isset($_POST[$art_start_date]))?update_user_meta($current_user->ID, $art_start_date, date(DATE_NUMERIC, strtotime(sanitize_text_field($_POST[$art_start_date])))): '';
    	(isset($_POST[$provinceILive]))?update_user_meta($current_user->ID, $provinceILive, sanitize_text_field($_POST[$provinceILive])): '';
    	(isset($_POST[$cityILive]))?update_user_meta($current_user->ID, $cityILive, sanitize_text_field($_POST[$cityILive])): '';
    	(isset($_POST[$institutionType]))?update_user_meta($current_user->ID, $institutionType, sanitize_text_field($_POST[$institutionType])): '';
    	(isset($_POST[$institutionName]))?update_user_meta($current_user->ID, $institutionName, sanitize_text_field($_POST[$institutionName])): '';
    	(isset($_POST[$onMedicalScheme]))?update_user_meta($current_user->ID, $onMedicalScheme, sanitize_text_field($_POST[$onMedicalScheme])): '';
    	if ($userEmail != $submitedMail) {
    		$user_data = wp_update_user(array('ID' => $current_user->ID, 'user_email' => $submitedMail));
    		if ( is_wp_error( $user_data ) ) {
    			$error['mail'] = 'Email already exist!';
    		}else{
    			$userEmail = $submitedMail;
    		}
    	}
    	if (isset($password) && !empty($password) && $password == $_POST['cpassword']) {
    		wp_set_password($password, $current_user->ID);
    	}
    }
    get_header();
    ?>
<!-- sidebar btns -->
<?php include_once('common/left_side_menu.php') ?>
<!-- sidebar btns -->
<!-- Registration form -->
<div class="col-lg-6 col-md-8 padding-delete">
    <div class="mid-section login-registration-form">
        <h3 class="div-title">Edit profile</h3>
        <hr>
        <?php
            // TO SHOW THE PAGE CONTENTS
            while (have_posts()) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
        <div class="entry-content-page">
            <?php the_content(); ?> <!-- Page Content -->
        </div>
        <!-- .entry-content-page -->
        <?php
            endwhile;
            wp_reset_query();
            ?>
        <form id='user_registration' method="post">
            <div class="form-group">
                <label class="text-center"><em>*Only a username and email address is required.</em></label>
                <input type="text" name="username" id="user_registration_username" value="<?php echo $current_user->user_login;?>" class="form-control" placeholder="User name. Doesn't have to be real name" readonly>
            </div>
            <div class="form-group custom-from-group">
                <label>Birth Date</label>
                
                <input id="dob" width= "250px" name="<?php echo $birth_date;?>" value="<?php echo date(DATE_NAME, strtotime($utils->getUserMeta($birth_date)));?>" readonly/>

            </div>
            <div class="form-group custom-form-select">
                <select class="select-1" name="<?php echo $gender; ?>">
                    <option disabled selected>Sex</option>
                    <option <?php echo ($utils->getUserMeta($gender) == 'Male') ? $selected : '' ;?>>Male</option>
                    <option <?php echo ($utils->getUserMeta($gender) == 'Female') ? $selected : '' ;?>>Female</option>
                </select>
                <select class="select-2" name="<?php echo $genderIdentity;?>">
                    <option class="option-align" value="">Gender identity (optional)</option>
                    <option <?php echo ($utils->getUserMeta($genderIdentity) == 'Agender') ? $selected : '' ;?>>Agender</option>
                    <option <?php echo ($utils->getUserMeta($genderIdentity) == 'Androgyne') ? $selected : '' ;?>>Androgyne</option>
                    <option <?php echo ($utils->getUserMeta($genderIdentity) == 'Cis Man') ? $selected : '' ;?>>Cis Man</option>
                    <option <?php echo ($utils->getUserMeta($genderIdentity) == 'Cis Woman') ? $selected : '' ;?>>Cis Woman</option>
                    <option <?php echo ($utils->getUserMeta($genderIdentity) == 'Demigender') ? $selected : '' ;?>>Demigender</option>
                    <option <?php echo ($utils->getUserMeta($genderIdentity) == 'Genderqueer/gender fluid') ? $selected : '' ;?>>Genderqueer/gender fluid</option>
                    <option <?php echo ($utils->getUserMeta($genderIdentity) == 'Questioning') ? $selected : '' ;?>>Questioning</option>
                    <option <?php echo ($utils->getUserMeta($genderIdentity) == 'Transgender man') ? $selected : '' ;?>>Transgender man</option>
                    <option <?php echo ($utils->getUserMeta($genderIdentity) == 'Transgender woman') ? $selected : '' ;?>>Transgender woman</option>
                </select>
            </div>rest-api/docs/#/endpoint/get_wp_v2_posts
            <div class="form-group custom-from-group">
                <label>Date I found out I am HIV+ </label>
                <input id="found_hiv" width= "250px" name="<?php echo $hiv_found_date;?>" value="<?php echo date(DATE_NAME, strtotime($utils->getUserMeta($hiv_found_date)));?>" readonly/>
               
            </div>
            <div class="form-group custom-from-group">
                <label>Date I started taking ART </label>
                <input id="start_art" width= "250px" name="<?php echo $art_start_date;?>" value="<?php echo date(DATE_NAME, strtotime($utils->getUserMeta($art_start_date)));?>" readonly/>
             
            </div>
            <div class="form-group custom-from-group">
                <label>Province where I live now</label>
                <select style=" width: 250px;"  name="<?php echo $provinceILive;?>">
                    <option disabled selected>Select province</option>
                    <option <?php echo ($utils->getUserMeta($provinceILive) == 'Eastern Cape') ? $selected : '' ;?>>Eastern Cape</option>
                    <option <?php echo ($utils->getUserMeta($provinceILive) == 'Free State') ? $selected : '' ;?>>Free State</option>
                    <option <?php echo ($utils->getUserMeta($provinceILive) == 'Gauteng') ? $selected : '' ;?>>Gauteng</option>
                    <option <?php echo ($utils->getUserMeta($provinceILive) == 'Kwazulu-Natal') ? $selected : '' ;?>>Kwazulu-Natal</option>
                    <option <?php echo ($utils->getUserMeta($provinceILive) == 'Limpopo') ? $selected : '' ;?>>Limpopo</option>
                    <option <?php echo ($utils->getUserMeta($provinceILive) == 'Mpumalanga') ? $selected : '' ;?>>Mpumalanga</option>
                    <option <?php echo ($utils->getUserMeta($provinceILive) == 'Northern Cape') ? $selected : '' ;?>>Northern Cape</option>
                    <option <?php echo ($utils->getUserMeta($provinceILive) == 'North West') ? $selected : '' ;?>>North West</option>
                    <option <?php echo ($utils->getUserMeta($provinceILive) == 'Western Cape') ? $selected : '' ;?>>Western Cape</option>
                </select>
            </div>
            <div class="form-group">
                <input type="text" name="<?php echo $cityILive;?>" class="form-control" placeholder="City or town where I live now" value="<?php echo $utils->getUserMeta($cityILive);?>">
            </div>
            <div class="form-group custom-from-group">
                <label>Where I get my ART from</label>
                <select  style=" width: 250px;" name="<?php echo $institutionType;?>">
                    <option disabled selected>Select kind of institution</option>
                    <option <?php echo ($utils->getUserMeta($institutionType) == 'Adherence club') ? $selected : '' ;?>>Adherence club</option>
                    <option <?php echo ($utils->getUserMeta($institutionType) == 'Government clinic') ? $selected : '' ;?>>Government clinic</option>
                    <option <?php echo ($utils->getUserMeta($institutionType) == 'Government hospital') ? $selected : '' ;?>>Government hospital</option>
                    <option <?php echo ($utils->getUserMeta($institutionType) == 'Private clinic/hospital/pharmacy') ? $selected : '' ;?>>Private clinic/hospital/pharmacy</option>
                    <option <?php echo ($utils->getUserMeta($institutionType) == 'General Practitioner') ? $selected : '' ;?>>General Practitioner</option>
                </select>
            </div>
            <div class="form-group">
                <input type="text" name="<?php echo $institutionName; ?>" class="form-control" placeholder="Name of clinic, hospital or pharmacy"  value="<?php echo $utils->getUserMeta($institutionName);?>">
            </div>
            <div class="form-group custom-from-group">
                <label>I am on a medical aid scheme</label>
                <select style=" width: 250px;" name="<?php echo $onMedicalScheme; ?>">
                    <option disabled selected> Select </option>
                    <option <?php echo ($utils->getUserMeta($onMedicalScheme) == 'Yes') ? $selected : '' ;?>>Yes</option>
                    <option <?php echo ($utils->getUserMeta($onMedicalScheme) == 'No') ? $selected : '' ;?>>No</option>
                </select>
            </div>
            <div class="form-group">
                <input type="email" name="email" class="form-control" placeholder="Email" value="<?php echo $userEmail;?>">
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Password">
            </div>
            <div class="form-group">
                <input type="password" name="cpassword" class="form-control" placeholder="Confirm password">
            </div>
            <input type="submit" name="edit_profile" value="Submit" class="btn learn-btn">
        </form>
        <div class="formsubmitError"></div>
    </div>
</div>
<!-- Registration form -->
<!-- app advertisement -->
<?php include_once('common/right_side_menu.php') ?>
<!-- app advertisement -->
<?php
    get_footer();
    ?>
<script>
    jQuery(document).ready(function() {
    	jQuery( '#user_registration' ).submit( function(e){
    		e.preventDefault();
    		
            if(!validateForm()){
    			jQuery('html, body').animate({
                    scrollTop: jQuery(".error_form").parent().offset().top
                }, 1000);

    		}else{
                
                document.getElementById("user_registration").submit();
    		}
    	});
    }); 

    function char_count(selector, values){
        if(values.length >= 51){
            jQuery('.error_form').remove();
            jQuery("#user_registration input[name="+selector+"]").val(values.slice(0,51));
            let errorAct = jQuery("#user_registration input[name="+selector+"]");
    		errorAct.parent().append("<p class='error_form'>Please Use Less than 50 Characters</p>");
        }else{
            jQuery('.error_form').remove();
        }
        // console.log(jQuery("#user_registration input[name="+selector+"]").val());
        // console.log(values);
        
    }
    jQuery(document).ready(function($){
        var today, today2, datepicker;
        today = new Date(new Date().getFullYear() - 13, new Date().getMonth(), new Date().getDate());
      $('#dob').datepicker({
        format: 'dd mmmm yyyy',
        minDate: '01 January 1915',
        maxDate:  today,
        uiLibrary: 'bootstrap4',
      });
      console.log($('#dob').val());
      today2 = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
      $('#found_hiv').datepicker({
        format: 'dd mmmm yyyy',
        minDate: function(){
        return $('#dob').val();
        },
        maxDate:  today2,
        uiLibrary: 'bootstrap4',
      });
      $('#start_art').datepicker({
        format: 'dd mmmm yyyy',
        minDate: '01 January 1915',
        maxDate:  today2,
        uiLibrary: 'bootstrap4',
      });
    });



    function validateForm(){
            jQuery('.error_form').remove();
            var username = jQuery("#user_registration input[name=username]").val();
            // var gender = jQuery("#user_registration select[name=gender]").val();
    		var email = jQuery("#user_registration input[name=email]").val();
    		var password = jQuery("#user_registration input[name=password]").val();
            var hiv_found_date = jQuery("#user_registration input[name=hiv_found_date]").val();
            var art_start_date = jQuery("#user_registration input[name=art_start_date]").val();
    		var cpassword = jQuery("#user_registration input[name=cpassword]").val();
            var city_i_live = jQuery("#user_registration input[name=city_i_live]").val();
            var institution_name = jQuery("#user_registration input[name=institution_name]").val();

    		var error = [];
            var userRegex = new RegExp('^\s*([0-9a-zA-Z]+)\s*$');
    		if(!username){
    			error['username'] = "Username must not be empty";
    		}
    		if(!email){
    			error['email'] = "Email must not be empty";
            }
            
            var pass = new RegExp('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$');

            if (password && !pass.test(password)) {
                error['password'] = "Please add one caps letter one small letter one numeric and one character in your password ";
            }
            if(password && password.length <= 6){
                error['password'] = "Password must atleast 6 character";
            }
            if(password && !password && !cpassword){
                error['password'] = "Password must not be empty";
            }
            if(password !== cpassword){
                error['cpassword'] = "Password must be match";
            }
            // if(!hiv_found_date){
            //     error['hiv_found_date'] = "Found hiv must not be empty"
            // }
            // if(!art_start_date){
            //     error['art_start_date'] = "Start art must not be empty"
            // }

            if(institution_name.length >= 50){
                error['institution_name'] = "Please use less than 50 characters"
            }
            if(city_i_live.length >= 50){
                error['city_i_live'] = "Please use less than 50 characters"
            }
    		if( Array.isArray(error) && !jQuery.isEmptyObject(error)){
    			for (var key of Object.keys(error)) {
                    if(key == 'gender_identity'){
                        var errorAct = jQuery("#user_registration select[name="+key+"]");
                    }else{
                        var errorAct = jQuery("#user_registration input[name="+key+"]");
                    }
    				//errorAct.addClass('error');
    				errorAct.parent().append("<p class='error_form'>"+error[key]+"</p>");
    			}
                return false;
            }else{
                return true;
            }
    }
</script>