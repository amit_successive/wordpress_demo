<?php 
    /* Template Name: Register */
    if(isset($_GET['login']) && isset($_GET['username']) && isset($_GET['password'])){
        $auth = wp_authenticate_username_password( NULL, $_GET['username'], $_GET['password']);
        if (!is_wp_error($auth)) {
            require_once('wp-blog-header.php'); 
            $user_login = $_GET['username']; 
            $user = get_user_by('login',$user_login);
            $user_id = $user->ID; 
            wp_set_current_user($user_id, $user_login);
            wp_set_auth_cookie($user_id); 
            do_action('wp_login', $user_login); 
        }
    }
    if (is_user_logged_in()) {
    	wp_redirect(home_url('dashboard'));
    	exit;
    }
    get_header();
    ?>
<!-- sidebar btns -->
<?php include_once('common/left_side_menu.php') ?>
<!-- sidebar btns -->
<!-- Registration form -->
<div class="col-lg-6 col-md-8 padding-delete">
    <div class="mid-section login-registration-form">
        <h3 class="div-title">Register a profile</h3>
        <hr>
        <p class="text-center">We will <span class="underline">never</span> share your information with anyone without your consent. We will only use it to improve this app and for our work in communities.</p>
        <form id='user_registration'>
            <div class="form-group custom-placeholder">
                <label class="text-center"><em>*Only a username and email address is required to register.</em></label>
                <input type="text" name="username" id="user_registration_username" class="form-control" onKeyUp="validateForm();" required >
                <div class="placeholder">
                    *Username.<span> Doesn't have to be real name</span>
                </div>
            </div>
            <div class="form-group custom-from-group">
                <label>Birth Date</label>
                    <input id="dob" width= "250px" name="birth_date" value="<?php echo date(DATE_NAME, strtotime('-13 years'));?>" readonly/>
            </div>
            <div class="form-group custom-form-select">
                <select class="select-1" name="gender">
                    <option disabled selected value="sex">Sex</option>
                    <option value="Male">Male</option>
                    <option value="Female">Female</option>
                </select>
                <select class="select-2" name="gender_identity">
                    <option class="option-align" value="">Gender identity (optional)</option>
                    <option value="Agender">Agender</option>
                    <option value="Androgyne">Androgyne</option>
                    <option value="Cis Man">Cis Man</option>
                    <option value="Cis Woman">Cis Woman</option>
                    <option value="Demigender">Demigender</option>
                    <option value="Genderqueer/gender fluid">Genderqueer/gender fluid</option>
                    <option value="Questioning">Questioning</option>
                    <option value="Transgender man">Transgender man</option>
                    <option value="Transgender woman">Transgender woman</option>
                </select>
            </div>
            <div class="form-group custom-from-group">
                <label>Date I found out I am HIV+</label>
                <input id="found_hiv" width= "250px" name="hiv_found_date" onChange="validateForm();" readonly />
                
            </div>
            <div class="form-group custom-from-group">
                <label>Date I started taking ART</label>
                <input id="start_art" width= "250px" name="art_start_date" onChange="validateForm();" readonly/>                
            </div>
            <div class="form-group custom-from-group">
                <label>Province where I live now</label>
                <select style=" width: 250px;"  name="province_i_live">
                    <option disabled selected>Select province</option>
                    <option>Eastern Cape</option>
                    <option>Free State</option>
                    <option>Gauteng</option>
                    <option>Kwazulu-Natal</option>
                    <option>Limpopo</option>
                    <option>Mpumalanga</option>
                    <option>Northern Cape</option>
                    <option>North West</option>
                    <option>Western Cape</option>
                </select>
            </div>
            <div class="form-group">
                <input type="text" name="city_i_live" class="form-control cityILive" placeholder="City or town where I live now" onKeyUp="char_count('city_i_live',this.value)">
            </div>
            <div class="form-group custom-from-group">
                <label>Where I get my ART from</label>
                <select  style=" width: 250px;" name="institution_type">
                    <option disabled selected>Select kind of institution</option>
                    <option>Adherence club</option>
                    <option>Government clinic</option>
                    <option>Government hospital</option>
                    <option>Private clinic/hospital/pharmacy</option>
                    <option>General Practitioner</option>
                </select>
            </div>
            <div class="form-group">
                <input type="text" name="institution_name" class="form-control clinicName" placeholder="Name of clinic, hospital or pharmacy"  onKeyup="char_count('institution_name' ,this.value)">
            </div>
            <div class="form-group custom-from-group">
                <label>I am on a medical aid scheme</label>
                <select style=" width: 250px;" name="on_medical_scheme">
                    <option disabled selected> Select </option>
                    <option>Yes</option>
                    <option>No</option>
                </select>
            </div>
            <div class="form-group placeholder-size">
                <input type="email" name="email" class="form-control" placeholder="*Email" onKeyUp="validateForm();" />
            </div>
            <div class="form-group placeholder-size">
                <input type="password" name="password" class="form-control" placeholder="*Password" onKeyUp="validateForm();" />
            </div>
            <div class="form-group placeholder-size">
                <input type="password" name="cpassword" class="form-control" placeholder="*Confirm password" onKeyUp="validateForm();" />
            </div>
            <button type="submit" class="btn learn-btn">Submit</button>
            <div id="loader"></div>
            <div class="formsubmitError"></div>
        </form>
        
    </div>
</div>
<!-- Registration form -->
<!-- app advertisement -->
<?php include_once('common/right_side_menu.php') ?>
<!-- app advertisement -->
<?php
    get_footer();
    ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script>
    jQuery(document).ready(function() { 
    	
    	jQuery( '#user_registration' ).submit( function(e){
            // alert(getVal('gender_identity','select'));
    		e.preventDefault();
    		if(!validateForm()){
                jQuery('html, body').animate({
                    scrollTop: jQuery(".error_form").parent().offset().top
                }, 1000);

    		}else{
                var username = jQuery("#user_registration input[name=username]").val();
                var email = jQuery("#user_registration input[name=email]").val();
                var password = jQuery("#user_registration input[name=password]").val();
    			postData = {
                    username : username,
                    email : email,
                    password : password,
                    meta : {
                        birth_date:getMonthFromString(getVal('birth_date')),
                        gender: getVal('gender','select'),
                        gender_identity: getVal('gender_identity','select'),
                        hiv_found_date: getMonthFromString(getVal('hiv_found_date')),
                        art_start_date: getMonthFromString(getVal('art_start_date')),
                        province_i_live: getVal('province_i_live','select'),
                        city_i_live: getVal('city_i_live'),
                        institution_type: getVal('institution_type','select'),
                        institution_name: getVal('institution_name'),
                        on_medical_scheme: getVal('on_medical_scheme','select'),
                    }
                }
    			console.log(postData);
                var spinner = $('#loader');
                spinner.show();
    			jQuery.ajax({
    				url : '<?php echo site_url("wp-json/wp/v2/signup");?>',
    				type : 'post',
    				data : postData,
    				success : function( response ) {
    					console.log(response);
    					if(response.success === 1){
    						window.location.href = '<?php site_url("/register/")?>?login=true&username='+username+'&password='+password;
                            spinner.hide();
                            
    					}else{
    						jQuery('.formsubmitError').html(response.message);
                            
    					}
    					
    				},
                    error: function (request, status, error) {
                        jQuery('.formsubmitError').html(request.responseJSON.message);
                        // alert(request.responseText);
                        spinner.hide();
                    }
    			});
    		}
    		
    		
    	});
    	
    function getVal(name, type = 'input'){
    	let valForReturn = jQuery("#user_registration "+type+"[name="+name+"]").val();
    	if(valForReturn){
    		return valForReturn;
    	}else{
    		return "";
    	}
    }
    
    }); 


    function char_count(selector, values){
        if(values.length >= 51){
            jQuery('.error_form').remove();
            jQuery("#user_registration input[name="+selector+"]").val(values.slice(0,51));
            let errorAct = jQuery("#user_registration input[name="+selector+"]");
    		errorAct.parent().append("<p class='error_form'>Please Use Less than 50 Characters</p>");
        }else{
            jQuery('.error_form').remove();
        }
        // console.log(jQuery("#user_registration input[name="+selector+"]").val());
        // console.log(values);
        
    }
    function getMonthFromString(date){
        var exisTime = Date.parse(date);
        return moment(exisTime).format("DD-MM-YYYY");
    }

    jQuery(document).ready(function($){
        var today, datepicker;
      today = new Date(new Date().getFullYear() - 13, new Date().getMonth(), new Date().getDate());
      $('#dob').datepicker({
        format: 'dd mmmm yyyy',
        minDate: '01 January 1915',
        maxDate: today,
        uiLibrary: 'bootstrap4',
       
      });
      
      $('#found_hiv').datepicker({
        format: 'dd mmmm yyyy',
        minDate: function(){
        return $('#dob').val();
        },
        maxDate:  today,
        uiLibrary: 'bootstrap4',
        
      });
      $('#start_art').datepicker({
        format: 'dd mmmm yyyy',
        minDate: '01 January 1915',
        uiLibrary: 'bootstrap4',
        
      });
    });

    function validateForm(){
            jQuery('.error_form').remove();
            var username = jQuery("#user_registration input[name=username]").val();
            // var gender = jQuery("#user_registration select[name=gender]").val();
    		var email = jQuery("#user_registration input[name=email]").val();
    		var password = jQuery("#user_registration input[name=password]").val();
            var hiv_found_date = jQuery("#user_registration input[name=hiv_found_date]").val();
            var art_start_date = jQuery("#user_registration input[name=art_start_date]").val();
    		var cpassword = jQuery("#user_registration input[name=cpassword]").val();
            var city_i_live = jQuery("#user_registration input[name=city_i_live]").val();
            var institution_name = jQuery("#user_registration input[name=institution_name]").val();
            getMonthFromString(hiv_found_date);
    		var error = [];
            var userRegex = new RegExp('^\s*([0-9a-zA-Z]+)\s*$');
    		if(!username){
    			error['username'] = "Username must not be empty";
            }

            if (!userRegex.test(username)) {
                error['username'] = "Username should have alpha-numeric character only.";
            }
            
            if(username.length < 3){
                error['username'] = "Username can have min 3 character";
            }

            if(username.length >= 16){
                error['username'] = "Username can have max 15 character";
            }
            // console.log(gender);
            // if(!gender){
            //     error['gender_identity']= "Gender is required";
            // }

    		if(!email){
    			error['email'] = "Email must not be empty";
    		}
           
            var pass = new RegExp('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$');

            if (!pass.test(password)) {
                error['password'] = "Please add one caps letter one small letter one numeric and one character in your password ";
            }
            if(password.length <= 5){
                error['password'] = "Password must atleast 6 character";
            }
    		if(!password && !cpassword){
    			error['password'] = "Password must not be empty";
    		}
    		if(password !== cpassword){
    			error['cpassword'] = "Password must be match";
    		}
            // if(!hiv_found_date){
            //     error['hiv_found_date'] = "Found HIV must not be empty"
            // }
            // if(!art_start_date){
            //     error['art_start_date'] = "Start art must not be empty"
            // }
            if(institution_name.length >= 50){
                error['institution_name'] = "Please use less than 50 characters"
            }
            if(city_i_live.length >= 50){
                error['city_i_live'] = "Please use less than 50 characters"
            }
    		if( Array.isArray(error) && !jQuery.isEmptyObject(error)){
    			for (var key of Object.keys(error)) {
                    if(key == 'gender_identity'){
                        var errorAct = jQuery("#user_registration select[name="+key+"]");
                    }else{
                        var errorAct = jQuery("#user_registration input[name="+key+"]");
                    }
    				//errorAct.addClass('error');
    				errorAct.parent().append("<p class='error_form'>"+error[key]+"</p>");
    			}
                return false;
            }else{
                return true;
            }
    }
   
     

</script>