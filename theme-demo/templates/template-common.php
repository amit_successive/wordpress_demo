<?php 
/* Template Name: Common */
get_header();
?>
<!-- sidebar btns -->
<?php include_once('common/left_side_menu.php'); ?>
<!-- latest news section-->

<div class="col-xl-6 col-lg-6 col-md-8 padding-delete">
    <div class="mid-section">
        <div class="row">
            <div class="mid-section-inner-wrapper">
            <?php
                if (get_field('common_enable_icon') == 'Yes') { ?>
                    <img src="<?php the_field('common_icon_image');?>" class="img-responsive" alt="Question-problem">
                <?php } ?>    
                <h3 class="div-title-1"><?php the_title(); ?></h3>
                <hr>
                <?php
                // TO SHOW THE PAGE CONTENTS
                while (have_posts()) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
                    <div class="entry-content-page">
                        <?php the_content(); ?> <!-- Page Content -->
                    </div><!-- .entry-content-page -->
                <?php
                endwhile;
                wp_reset_query();
                ?>
                <!-- about page image and buttons - start -->
                <?php
                    if (get_field('enable_after_content_images') == 'Yes') { 
                        foreach (get_field('after_content_images') as $key => $value) { ?>
                            <img src="<?php echo $value['common_content_image'];?>" class="img-responsive" alt="Image">
                        <?php } ?>
                    <?php }
                ?>
                <?php
                    if (get_field('enable_after_content_buttons') == 'Yes') { ?>
                        <div class="mid-section-btn">
                        <?php foreach (get_field('after_content_button') as $key => $value) { ?>
                            <a class="btn custom-btn1" href="<?php echo $value['common_content_button_url'];?>">
                            <?php echo $value['common_content_button_text'];?>
                            </a>
                        <?php } ?>
                        </div>
                    <?php }
                ?>
                <!-- about page image and buttons -end -->
            </div>
        </div>
    </div>
</div>
<!-- app advertisement -->
<?php include_once('common/right_side_menu.php');?>
<!-- app advertisement -->
<?php
get_footer();
?>