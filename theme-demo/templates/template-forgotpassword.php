<?php 
    /* Template Name: Forgot Password */
    if (is_user_logged_in()) { wp_redirect(home_url('dashboard')); exit; }
    
    if (isset( $_POST['forgot_password'] )) {
        if(wp_verify_nonce( $_POST['fp_wpnonce'], 'fp_wpnonce' )){
            $userClass = new UserClass();
            $chk = $userClass->changeReset($_POST['forgot_password']);
            $msgFromForm = $chk['message'];
        } else {
            $msgFromForm = "invalid submission.";
        }
    }
    get_header();
    ?>
<!-- sidebar btns -->
<?php include_once('common/left_side_menu.php') ?>
<!-- latest news section-->
<!-- Forget form -->
<div class="col-lg-6 col-md-8 padding-delete">
    <div class="mid-section login-registration-form">
        <h3 class="div-title">Forgot Password</h3>
        <hr>
        <p><em>
        <?php
        if(!empty($msgFromForm)){
            echo $msgFromForm;
        }
        ?>
        </em></p>
        <form method="post">
        <?php wp_nonce_field( 'fp_wpnonce', 'fp_wpnonce' ); ?>
            <div class="form-group">
                <input type="email" name="forgot_password" class="form-control" placeholder="Email" required >
            </div>
            <button type="submit" class="btn custom-btn1">Submit</button>
        </form>
    </div>
</div>
<!-- Forget form -->
<!-- app advertisement -->
<?php include_once('common/right_side_menu.php') ?>
<!-- app advertisement -->
<?php get_footer(); ?>