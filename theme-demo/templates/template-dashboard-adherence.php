<?php 
/* Template Name: dashboard-adherence */
get_header();
?>
<!-- sidebar dashboard -->
<?php include_once('common/dashboard_side_menu.php'); ?>
<!-- sidebar dashboard -->
<?php
$userId =get_current_user_id();
$currentStreak = $res['adherenceCount'];
$adherence = new Adherence();
$highestStreak = $adherence->getAdherenceHighSteak($userId);
$adherenceDaysData = $adherence->getAdherenceDaysData($userId);
// print_r($adherenceDaysData );
$percentData = ($adherenceDaysData['currentDays'] != 0 )? number_format($adherenceDaysData['currentDays']*100/(($adherenceDaysData['totalDays'] != 0) ? $adherenceDaysData['totalDays'] : 1)) : 0;
?>
<!-- latest news section-->
		<div class=" col-xl-6 col-lg-6 col-md-8 col-7">
			<div class="tab-content mid-section-inner-wrapper" id="v-pills-tabContent">
				<div class="tab-pane fade show active dashboard-content" id="adherence" role="tabpanel" aria-labelledby="v-pills-home-tab">
					<img src="<?php echo TEMPLATE_URL; ?>/assets/images/calender.png" class="img-responsive" alt="Image">
					<h3 class="div-title-1"><?php the_title(); ?></h3>
					<hr>
					<p><em>Adherence means taking your ART as recommended. It is a very important part of managing your HIV.</em></p>
					<?php
                // TO SHOW THE PAGE CONTENTS
                while (have_posts()) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
                    <div class="entry-content-page">
                        <?php the_content(); ?> <!-- Page Content -->
                    </div><!-- .entry-content-page -->
                <?php
                endwhile;
                wp_reset_query();
                ?>
					
					<div class="row">
						<div class="col-lg-6 col-md-7 padding-delete">
							<div class="dashboardRightWrapper right-side-wrapper mx-auto">
								<h5><?php echo $currentStreak; ?></h5>
							</div>
							<p class="dasboard-text">The number of days I have taken my ART since starting to use this app.</p>
							<hr>
							<p class="text-center"><?php echo sprintf('I have taken my ARVs %s days out of the %s days I needed to.', $adherenceDaysData['currentDays'], $adherenceDaysData['totalDays']);?></p>
							<p class="chart">
								<span id="count_chart"></span>
								
								<span class="chart-count">That is <span><?php echo $percentData;?>% </span>adherence.</span>
							</p>
							<ul>
								<li>
									Which is <span>GOOD.</span> Keep it up!
									<i class="fa fa-thumbs-up" aria-hidden="true"></i>
								</li>
								<li>
									My current streak (days in a row) <span><?php echo $currentStreak ;?></span>
								</li>
								<li>
									My highest streak record <span><?php echo $highestStreak;?></span>
								</li>
							</ul>
						</div>
						<div class="col-lg-6 col-md-5 dashboard-button">
						<?php 
							if(get_field('button_group')){
							foreach (get_field('button_group') as $key => $value) { 
								if($value['button_visible_for'] == 'Logged in User'  && is_user_logged_in()){
									?>
										<a class="btn custom-btn1" href="<?php echo $value['button_url'];?>" data-logged="true">
										<?php echo $value['button_title'];?>
										</a>
									<?php 
								}elseif ($value['button_visible_for'] == 'Logged Out User'  && !is_user_logged_in()) {
									?>
										<a class="btn custom-btn1" href="<?php echo $value['button_url'];?>" data-logged="false">
										<?php echo $value['button_title'];?>
										</a>
									<?php 
								}elseif ($value['button_visible_for'] == 'Both User'){
									?>
									<a class="btn custom-btn1" href="<?php echo $value['button_url'];?>" data-logged="both">
									<?php echo $value['button_title'];?>
									</a>
								<?php 
								}

								}
							} ?>
						</div>
					</div>
				</div>
			</div>
		</div>


<?php
$googleChart = new GoogleChart();
$googleChart->circle('count_chart', $percentData, (int)(100-$percentData));
?>


<!-- app advertisement -->
<?php include_once('common/right_side_menu.php');?>
<!-- app advertisement -->
<?php
get_footer();
?>