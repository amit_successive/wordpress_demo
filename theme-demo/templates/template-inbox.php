<?php 
/* Template Name: Inbox */
get_header();
?>
<!-- sidebar btns -->
<?php include_once('common/left_side_menu.php') ?>

<!-- mid section-->
<div class="col-xl-6 col-lg-6 col-md-8 padding-delete">
    <div class="mid-section">
        <div class="row">
            <div class="mid-section-inner-wrapper inbox">
                <img src="assets/images/message-icon.png" class="img-responsive" alt="Image">
                <h3 class="div-title-1">Inbox</h3>
                <hr>
                <p>
                    Here you will see short alerts about important news regarding ART and this website/app.
                </p>
                <ul class="message-list">
                    <?php 
                    $query = new WP_Query( array('post_type'=>'inbox',  'order' => 'DESC',
                    'posts_per_page' => 5) );
                        if ( $query->have_posts() ) {
                            while ( $query->have_posts()) {
                            $query->the_post(); 
                    ?>
                    <li class="message-item">
                        <p><?php the_title()?></p>
                        <p><?php echo date('d F Y, g:i a', strtotime( $post->post_date));?></p>
                        <p><?php echo substr(strip_tags(get_the_content()),0,220).'...'; ?></p>
                    </li>
                 <?php }
                    }?>
                </ul>
            </div>
        </div>
    </div>
</div>

<!-- app advertisement -->
<?php include_once('common/right_side_menu.php') ?>
<!-- app advertisement -->
<?php
get_footer();
?>