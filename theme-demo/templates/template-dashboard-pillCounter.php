<?php 
/* Template Name: dashboard-pillCounter */
get_header();
?>
<!-- sidebar dashboard -->
<?php include_once('common/dashboard_side_menu.php'); ?>
<!-- sidebar dashboard -->
<!--pill counter section-->
		<div class=" col-xl-6 col-lg-6 col-md-8 col-7">
			<div class="tab-content mid-section-inner-wrapper" id="v-pills-tabContent">
				<div class="tab-pane fade show active dashboard-content" id="adherence" role="tabpanel" aria-labelledby="v-pills-home-tab">
					<img src="<?php echo TEMPLATE_URL; ?>/assets/images/pills.png" class="img-responsive" alt="Image">
					<h3 class="div-title-1"><?php the_title(); ?></h3>
					<hr>
					<p><em>This tool helps you keep track of how many ARVs you have left and reminds you to restock.</em></p>
					<div class="row">
						<div class="col-lg-6 col-md-7 padding-delete">
							<div class="dashboardRightWrapper right-side-wrapper mx-auto">
								<h5><?php echo $res['pillCountDays'];?></h5>
							</div>
							<!--<p class="dasboard-text">The amount of days of ART I have left.</p>-->
							<p class="bg-yellow text-center">
								<strong><?php echo $res['pillsMsg'];?></strong>
							</p>
							<ul class="pillCounter">
								<li>
									Amount of pills I get when I restock
									<br>
									<span><?php echo $res['pillsIGetsRestock'];?> pills</span>
								</li>
								<li>
									How many pills I take every day <br>
									<span><?php echo $res['pillsITakeRestock'];?> pills</span>
								</li>
								<li>
									Remind me daily when<br>
									<span>I have <?php echo $res['remindDay'];?> days of ARVs left</span>
								</li>
								<li>
									Remind me at <br><span><?php echo $res['remindTime'];?></span>
								</li>
							</ul>
						</div>
						<div class="col-lg-6 col-md-5 dashboard-button">
						<?php 
							if(get_field('button_group')){
							foreach (get_field('button_group') as $key => $value) { 
								if($value['button_visible_for'] == 'Logged in User'  && is_user_logged_in()){
									?>
										<a class="btn custom-btn1" href="<?php echo $value['button_url'];?>" data-logged="true">
										<?php echo $value['button_title'];?>
										</a>
									<?php 
								}elseif ($value['button_visible_for'] == 'Logged Out User'  && !is_user_logged_in()) {
									?>
										<a class="btn custom-btn1" href="<?php echo $value['button_url'];?>" data-logged="false">
										<?php echo $value['button_title'];?>
										</a>
									<?php 
								}elseif ($value['button_visible_for'] == 'Both User'){
									?>
									<a class="btn custom-btn1" href="<?php echo $value['button_url'];?>" data-logged="both">
									<?php echo $value['button_title'];?>
									</a>
								<?php 
								}

								}
							} ?>
						</div>
					</div>
				</div>
				
			</div>
		</div>

<!-- app advertisement -->
<?php include_once('common/right_side_menu.php');?>
<!-- app advertisement -->
<?php
get_footer();
?>