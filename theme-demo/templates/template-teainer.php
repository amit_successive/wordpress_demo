<?php 
/* Template Name: trainer */
get_header();
?>
<!-- sidebar btns -->
<?php include_once('common/left_side_menu.php'); ?>
<!-- latest news section-->
<div class="col-xl-9 col-lg-9 col-md-8 d-flex padding-delete">
    <div class="mid-section">
        <div class="row">
            <div class="trainer">
                <?php
                // TO SHOW THE PAGE CONTENTS
                while (have_posts()) : the_post(); ?> 
                <!--Because the_content() works only inside a WP Loop -->
                    <?php the_content(); ?>
                    <!-- Page Content -->
                <?php
                    endwhile;
                    wp_reset_query();
                ?>
            </div>
        </div>
    </div>
</div>
<!-- footer -->
<?php
get_footer();
?>