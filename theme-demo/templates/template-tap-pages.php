<?php 
	/* Template Name: Tab-pages */
	get_header();
	?>
<!-- sidebar btns -->
<?php include_once('common/left_side_menu.php'); ?>
<!-- sidebar btns -->
<!-- mid section -->
<div class="col-xl-9 col-lg-9 col-md-8 d-flex padding-delete">
<div class="mid-section tab-pages-section padding-delete">
	<div class="row custom-row">
		<div class="col-xl-3 col-lg-4 col-md-4 col-5  padding-delete">
			<div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
				<?php 
					if(get_field('accordion_section')){
					    $countKey = 0;
					    foreach (get_field('accordion_section') as $key => $value){
					    if(in_array('Web', $value['visibility'])){
					    $countKey++;
					    ?>
				<a class="nav-link <?php echo ($countKey == 1) ? 'active' : '';?>" id="<?php echo $value['id_name'];?>-tab" data-toggle="pill" href="#<?php echo $value['id_name'];?>" role="tab" aria-controls="<?php echo $value['id_name'];?>" aria-selected="true"><?php echo trim($value['accordian_title']);?></a>
				<?php }
					}
					}
					?>
			</div>
		</div>
		<div class="col-xl-9 col-lg-8 col-md-8 col-7">
			<div class="tab-content" id="v-pills-tabContent">
				<?php 
					if (get_field('tab_image')) {?>
				<img src="<?php the_field('tab_image')?>" class="img-responsive" alt="Image">
				<?php }?>
				<?php
					if(get_field('accordion_section')){
					    $countVal = 0;
					    foreach(get_field("accordion_section") as $key => $value){
					    if(in_array('Web', $value['visibility'])){
						$countVal++;
						$titleCount = 1;
					?>
				<div class="tab-pane fade <?php echo ($countVal == 1) ? 'show active' : $countVal;?>" id="<?php echo $value['id_name'];?>" role="tabpanel" aria-labelledby="<?php echo $value['id_name'];?>-tab">
					<div class="tab-content-wrapper inner_page">
						
						<?php 
							if(!empty($value['accordian_detail_wrapper']) && $value['accordian_detail_wrapper']){
							    foreach($value['accordian_detail_wrapper'] as $key1 => $value1 ){ 
								?>
						<div class="row">
							<div class="col-xl-6 col-lg-7 padding-delete">
							<?php if($titleCount === 1){ ?>
								<h2 class="title"><?php echo $value['accordian_title_right'];?></h2>
							<?php }
							$titleCount = $titleCount+1;
							?>
								<?php echo str_replace("&nbsp;", "", $value1['accordian_detail']); ?>
							</div>
							<?php 
								if($value1['accordian_image']){
								?>
							<div class="col-xl-6 col-lg-5">
								<img src="<?php echo $value1['accordian_image'] ?>" alt="tab image">
							</div>
							<?php }?>
						</div>
                        <?php }
                    } ?>   
					</div>
				</div>
				<?php }
					}
					}
					
					?>
				<a href="#" class="btn btnprevious">
				<i class="fa fa-caret-left" aria-hidden="true"></i>
				</a>
				<a href="#" class="btn btnnext">
				<i class="fa fa-caret-right" aria-hidden="true"></i>
				</a>
			</div>
		</div>
	</div>
</div>
<!-- mid section -->
<?php
	get_footer();
	?>