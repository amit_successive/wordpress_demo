<?php 
/* Template Name: dashboard-cd4-count */
get_header();
?>
<!-- sidebar dashboard -->
<?php include_once('common/dashboard_side_menu.php'); ?>
<!-- sidebar dashboard -->
<!--pill counter section-->
		<div class=" col-xl-6 col-lg-6 col-md-8 col-7">
			<div class="tab-content mid-section-inner-wrapper" id="v-pills-tabContent">
				<div class="tab-pane fade show active dashboard-content" id="adherence" role="tabpanel" aria-labelledby="v-pills-home-tab">
					<img src="<?php echo TEMPLATE_URL; ?>/assets/images/cd4.png" class="img-responsive" alt="Image">
					<h3 class="div-title-1"><?php the_title(); ?></h3>
                    <hr>
                    <p><em>CD4 cells are a type of white blood cells that fight infection. CD4 tests measure the number of these cells in your blood.</em></p>
                    <div class="row">
                        <div class="col-lg-6 col-md-7 padding-delete">
                            <div class="dashboardRightWrapper right-side-wrapper mx-auto">
                                <h5><?php echo $res['cd4Count'];?></h5>
                            </div>
                            <p class="dasboard-text">Is my CD4 Count.</p>
                            <ul class="CD4Count">
                                <li>
                                    My last test was on<br><span><?php echo ($res['cd4latestTest'] != 0) ? date_format(date_create($res['cd4latestTest']),"d F Y") : "dd month yy"; ?></span>
                                </li>
                                <li>
                                    My next test is on<br><span><?php echo ($res['cd4nextTest'] != 0) ? date_format(date_create($res['cd4nextTest']),"d F Y") : "dd month yy"; ?></span>
                                </li>
                            </ul>
                            <br>
                            <p class="text-center">My CD4 count graph</p>
                            <div id="cd4_count"></div>
                        </div>
                        <div class="col-lg-6 col-md-5 dashboard-button">
                        <?php 
							if(get_field('button_group')){
							foreach (get_field('button_group') as $key => $value) { 
								if($value['button_visible_for'] == 'Logged in User'  && is_user_logged_in()){
									?>
										<a class="btn custom-btn1" href="<?php echo $value['button_url'];?>" data-logged="true">
										<?php echo $value['button_title'];?>
										</a>
									<?php 
								}elseif ($value['button_visible_for'] == 'Logged Out User'  && !is_user_logged_in()) {
									?>
										<a class="btn custom-btn1" href="<?php echo $value['button_url'];?>" data-logged="false">
										<?php echo $value['button_title'];?>
										</a>
									<?php 
								}elseif ($value['button_visible_for'] == 'Both User'){
									?>
									<a class="btn custom-btn1" href="<?php echo $value['button_url'];?>" data-logged="both">
									<?php echo $value['button_title'];?>
									</a>
								<?php 
								}

								}
							} ?>
                        </div>
                    </div>
				</div>
				
			</div>
		</div>
<?php
$graphData = $cd4Count->getGraphData(get_current_user_id());
$googleChart = new GoogleChart();
$googleChart->viralLoadGraph('cd4_count', $graphData);
?>






<!-- app advertisement -->
<?php include_once('common/right_side_menu.php');?>
<!-- app advertisement -->
<?php
get_footer();
?>