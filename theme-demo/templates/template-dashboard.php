<?php 
/* Template Name: dashboard */
get_header();
include_once('common/dashboard_data.php');

?>
<!-- sidebar btns -->
<?php include_once('common/left_side_menu.php'); ?>
<!-- latest news section-->

<div class="col-xl-6 col-lg-6 col-md-8 padding-delete">
	<div class="mid-section">
		<div class="row">
			<div class="mid-section-inner-wrapper">
				<img src="<?php echo TEMPLATE_URL; ?>/assets/images/dashboard.png" class="img-responsive" alt="Image">
				<h3 class="div-title-1">Dashboard</h3>
				<hr>
				<p>
					Click on a section to see more. You need to download the app to use these tools.
				</p>
				<div class="dashboard-items">
					<ul>
						<li>
							<a href="<?php echo $res['nameTabUrl']?>" title="<?php echo $res['linkText'];?>"  class="row align-items-center">
								<div class="right-side-wrapper">
									<img src="<?php echo $res['userImage']; ?>" class="img-responsive" alt="Profile">
								</div>
								<div class="left-side-wrapper">
									<h5><?php echo $res['name']; ?></h5>
									<p>On Art since <?php echo $res['artSince']; ?></p>
								</div>
							</a>
						</li>
						<li>
							<a href="<?php echo $res['adherenceUrl']?>"  title="<?php echo $res['linkText'];?>" class="row align-items-center">
								<div class="right-side-wrapper d-flex align-items-center">
									<h5><?php echo $res['adherenceCount']; ?></h5>
								</div>
								<div class="left-side-wrapper">
									<h5>My adherence</h5>
									<p>How many days have I taken my ARVs?</p>
								</div>
							</a>
						</li>
						<li>
							<a href="<?php echo $res['pillsCounterUrl']?>"  title="<?php echo $res['linkText'];?>" class="row align-items-center">
								<div class="right-side-wrapper">
									<h5><?php echo $res['pillCountDays']; ?></h5>
								</div>
								<div class="left-side-wrapper">
									<h5>My pill counter</h5>
									<p>How many days of ARVs do I have left?</p>
								</div>
							</a>
						</li>
						<li>
							<a href="<?php echo $res['viralLoadUrl'];?>"  title="<?php echo $res['linkText'];?>" class="row align-items-center">
								<div class="right-side-wrapper ">
									<h5><?php echo $res['myViralLoadCount']; ?></h5>
								</div>
								<div class="left-side-wrapper">
									<h5>My viral load</h5>
									<p>
										Latest test: <?php echo ($res['latestTest'] != 0) ? date_format(date_create($res['latestTest']),"d F Y") : "dd month yy"; ?>
									</p>
									<p>
										Next test: <?php echo ($res['nextTest'] != 0) ? date_format(date_create($res['nextTest']),"d F Y") : "dd month yy"; ?>
									</p>
								</div>
							</a>
						</li>
						<li>
							<a href="<?php echo $res['cd4CounterUrl'];?>" title="<?php echo $res['linkText'];?>" class="row align-items-center" >
								<div class="right-side-wrapper">
									<h5><?php echo $res['cd4Count']; ?></h5>
								</div>
								<div class="left-side-wrapper">
									<h5>My CD4 count</h5>
									<p>
										Latest test: <?php echo ($res['cd4latestTest'] != 0) ? date_format(date_create($res['cd4latestTest']),"d F Y") : "dd month yy"; ?>
									</p>
									<p>
										Next test: <?php echo ($res['cd4nextTest'] != 0) ? date_format(date_create($res['cd4nextTest']),"d F Y") : "dd month yy"; ?>
									</p>
								</div>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- app advertisement -->
<?php include_once('common/right_side_menu.php');?>
<!-- app advertisement -->
<?php
get_footer();
?>
<?php 
if(isset($_GET['delete']) && $_GET['delete'] == "success"){ ?>
	<script>
	jQuery(document).ready(function($){
		deleteSuccess('<?php echo site_url();?>'+'/dashboard');
	})
	</script>
<?php } ?>