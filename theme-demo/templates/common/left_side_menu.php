<?php 
    global $post;
    $current_page_slug = $post->post_name;
    $menu_name = 'secondary';
    $locations = get_nav_menu_locations();
    $menu = wp_get_nav_menu_object($locations[$menu_name]);
    $menuitems = wp_get_nav_menu_items( $menu->term_id, array( 'order' => 'DESC' ));
    $resMenu = array();
    $isMenuShow = false;
    foreach ( $menuitems as $item ) {
        array_push($resMenu, $item->url);
    }
    foreach ($resMenu as $url) {
        if (strpos($url, '/'.$current_page_slug) !== FALSE) {
            $isMenuShow = true;
        }
    }
?>
<div class="col-xl-3 col-lg-3 col-md-4 d-none d-md-block padding-delete bg-gray">
    <div class="learn-more">
        <h3 class="div-title text-center">Learn More</h3>
        <div class="buttons">
            <?php
                foreach ( $menuitems as $item ):
                    $id = get_post_meta( $item->ID, '_menu_item_object_id', true );
                    $page = get_page( $id );
                    $link = get_page_link( $id ); ?>
                <a href="<?php echo $link; ?>" class="btn learn-btn 
                <?php echo (get_field('enable_menu_item', $item) == 'no')? 'disable':'';?>
                <?php if($isMenuShow){
                    echo (strpos($item->url, '/'.$current_page_slug) == false)? 'fadeColor':'';
                }?>
                ">
                <h6><?php echo $item->title; ?></h6>
                <p><?php the_field('sub_menu_feild', $item);?></p>
            </a>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<!-- learn more buttons for mobile -->
<div class="d-md-none d-sm-block col-12">
    <p>
        <a class="btn learn-btn learn-mobile-btn" data-toggle="collapse" href="#md-laern-btn" role="button" aria-expanded="false" aria-controls="md-laern-btn">
        Learn More<span><i class="fa fa-caret-down" aria-hidden="true"></i></span>
        </a>
    </p>
    <div class="collapse learn-more" id="md-laern-btn">
        <div class="buttons">
            <?php
                foreach ( $menuitems as $item ) {
                $id = get_post_meta( $item->ID, '_menu_item_object_id', true );
                $page = get_page( $id );
                $link = get_page_link( $id ); ?>
            <a href="<?php echo $link; ?>" class="btn learn-btn">
                <h6><?php echo $page->post_title; ?></h6>
                <p><?php the_field('sub_menu_feild', $item);?></p>
            </a>
			<?php } ?>
        </div>
    </div>
</div>
