<?php
include_once('dashboard_data.php');
?>
<div class="col-lg-3 col-md-4 col-5 bg-gray dashboard-padding">
   <div class="dashboard">
      <a href="<?php echo site_url('/dashboard/'); ?>">
         <img src="<?php echo TEMPLATE_URL; ?>/assets/images/dashboard.png" class="img-responsive dashboard-icon" alt="Image">
         <h3 class="div-title-1">Dashboard</h3>
      </a>
      <p>
         Click on a section to see more or edit.
      </p>
      <div class="nav flex-column nav-pills dashboard-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
         <a href="<?php echo $res['nameTabUrl']?>">
            <div class="right-side-wrapper">
               <img src="<?php echo $res['userImage']?>" class="img-responsive" alt="Profile">
            </div>
            <div class="left-side-wrapper">
               <h5><?php echo $res['name']; ?></h5>
               <p>On Art since <?php echo $res['artSince']; ?></p>
            </div>
         </a>
         <a href="<?php echo $res['adherenceUrl']?>" class="nav-link">
            <div class="right-side-wrapper">
               <h5><?php echo $res['adherenceCount']; ?></h5>
            </div>
            <div class="left-side-wrapper">
               <h5>My adherence</h5>
               <p>How many days have I taken my ARVs?</p>
            </div>
         </a>
         <a class="nav-link" href="<?php echo $res['pillsCounterUrl']?>" >
            <div class="right-side-wrapper">
               <h5><?php echo $res['pillCountDays']; ?></h5>
            </div>
            <div class="left-side-wrapper">
               <h5>My pill counter</h5>
               <p>How many days of ARVs do I have left?</p>
            </div>
         </a>
         <a class="nav-link" href="<?php echo $res['viralLoadUrl'];?>" >
            <div class="right-side-wrapper ">
               <h5><?php echo $res['myViralLoadCount']; ?></h5>
            </div>
            <div class="left-side-wrapper">
               <h5>My viral load</h5>
               <p>
                  Latest test: <?php echo ($res['latestTest'] != 0) ? date_format(date_create($res['latestTest']),"d F Y") : "dd month yy"; ?>
               </p>
               <p>
                  Next test: <?php echo ($res['nextTest'] != 0) ? date_format(date_create($res['nextTest']),"d F Y") : "dd month yy"; ?>
               </p>
            </div>
         </a>
         <a class="nav-link" href="<?php echo $res['cd4CounterUrl'];?>">
            <div class="right-side-wrapper ">
               <h5><?php echo $res['cd4Count']; ?></h5>
            </div>
            <div class="left-side-wrapper">
               <h5>My CD4 Count</h5>
               <p>
                  Latest test: <?php echo ($res['cd4latestTest'] != 0) ? date_format(date_create($res['cd4latestTest']),"d F Y") : "dd month yy"; ?>
               </p>
               <p>
                  Next test: <?php echo ($res['cd4nextTest'] != 0) ? date_format(date_create($res['cd4nextTest']),"d F Y") : "dd month yy"; ?>
               </p>
            </div>
         </a>
      </div>
   </div>
</div>