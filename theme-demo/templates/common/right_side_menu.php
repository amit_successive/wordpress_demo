<?php
/**
 * Right Side Menu
 */
?>
<div class="col-xl-3 col-lg-3 col-md-12 mx-auto padding-delete bg-gray">
    <div class="get-free-app">
        <div class="socialmediaIcons">
            <h3 class="div-title-1">Follow Us... </h3>
            <a href="https://twitter.com/modernart4sa" target="_blank">
                    <img src="<?php echo TEMPLATE_URL; ?>/assets/images/twitter.svg" alt="twitter" class="img-responsive"/>
                </a>

                <a href="https://www.facebook.com/modernARTforSouthAfrica" target="_blank">
                    <img src="<?php echo TEMPLATE_URL; ?>/assets/images/fb.svg" alt="facebook" class="img-responsive"/> 
                </a>

                <a href="https://www.instagram.com/modernart4sa/" target="_blank">
                    <img src="<?php echo TEMPLATE_URL; ?>/assets/images/insta.svg" alt="insta" class="img-responsive"/>
                </a>

                <a href="https://www.youtube.com/channel/UCAddFt3giXa7SZV47wOz5kQ" target="_blank">
                    <img src="<?php echo TEMPLATE_URL; ?>/assets/images/youtube.svg" alt="youtube" class="img-responsive"/>
                </a>
            
    </div>
        <img src="<?php echo wp_get_attachment_url(get_option( 'modern_art_settings_right_menu_image' )[0]);?>" class="img-responsive" alt="img-responsive">
        <h3 class="div-title-1 text-center"><?php echo get_option( 'modern_art_settings_right_menu_title' );?></h3>
        <div class="get-free-app-content">
            <?php echo get_option( 'modern_art_settings_right_menu_description' );?>
            <a href="<?php echo get_option( 'modern_art_settings_right_menu_ios_link' );?>" class="btn custom-btn1">Get the free iOS app</a>
            <a href="<?php echo get_option( 'modern_art_settings_right_menu_android_link' );?>" class="btn custom-btn1">Get the free Android app</a>
        </div>
    </div>
</div>