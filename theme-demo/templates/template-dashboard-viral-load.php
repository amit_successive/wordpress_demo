<?php 
/* Template Name: dashboard-viral-loads */
get_header();
?>
<!-- sidebar dashboard -->
<?php include_once('common/dashboard_side_menu.php'); ?>
<!-- sidebar dashboard -->
<!--pill counter section-->
		<div class=" col-xl-6 col-lg-6 col-md-8 col-7">
			<div class="tab-content mid-section-inner-wrapper" id="v-pills-tabContent">
				<div class="tab-pane fade show active dashboard-content" id="adherence" role="tabpanel" aria-labelledby="v-pills-home-tab">
					<img src="<?php echo TEMPLATE_URL; ?>/assets/images/viral.png" class="img-responsive" alt="Image">
					<h3 class="div-title-1"><?php the_title(); ?></h3>
                        <hr>
                        <?php
                        // TO SHOW THE PAGE CONTENTS
                        while (have_posts()) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
                            <div class="entry-content-page">
                                <?php the_content(); ?> <!-- Page Content -->
                            </div><!-- .entry-content-page -->
                        <?php
                        endwhile;
                        wp_reset_query();
                        ?>
                        <div class="row">
                            <div class="col-lg-6 col-md-7 padding-delete">
                                <div class="dashboardRightWrapper right-side-wrapper mx-auto">
                                    <h5><?php echo $res['myViralLoadCount'];?></h5>
                                </div>

                                <p class="dasboard-text">Is my viral load.</p>
                                <ul class="viralLoad">
                                    <li>
                                        My last test was on<br><span><?php echo ($res['latestTest'] != 0) ? date_format(date_create($res['latestTest']),"d F Y") : "dd month yy"; ?></span>
                                    </li>
                                    <li>
                                        My next test is on<br><span><?php echo ($res['nextTest'] != 0) ? date_format(date_create($res['nextTest']),"d F Y") : "dd month yy"; ?></span>
                                    </li>
                                </ul>
                                <br>
                                <p class="text-center">My viral load graph</p>
                                <div id="viral-count-load"></div>
                            </div>
                            <div class="col-lg-6 col-md-5 dashboard-button">
                            <?php 
							if(get_field('button_group')){
							foreach (get_field('button_group') as $key => $value) { 
								if($value['button_visible_for'] == 'Logged in User'  && is_user_logged_in()){
									?>
										<a class="btn custom-btn1" href="<?php echo $value['button_url'];?>" data-logged="true">
										<?php echo $value['button_title'];?>
										</a>
									<?php 
								}elseif ($value['button_visible_for'] == 'Logged Out User'  && !is_user_logged_in()) {
									?>
										<a class="btn custom-btn1" href="<?php echo $value['button_url'];?>" data-logged="false">
										<?php echo $value['button_title'];?>
										</a>
									<?php 
								}elseif ($value['button_visible_for'] == 'Both User'){
									?>
									<a class="btn custom-btn1" href="<?php echo $value['button_url'];?>" data-logged="both">
									<?php echo $value['button_title'];?>
									</a>
								<?php 
								}

								}
							} ?>
                            </div>
                        </div>
				</div>
				
			</div>
		</div>
<?php
    $graphData = $viralLoad->getGraphData(get_current_user_id());
    $googleChart = new GoogleChart();
    $googleChart->viralLoadGraph('viral-count-load', $graphData);
?>
<!-- app advertisement -->
<?php include_once('common/right_side_menu.php');?>
<!-- app advertisement -->
<?php
get_footer();
?>