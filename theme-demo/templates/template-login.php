<?php 
    /* Template Name: Login */
    if (is_user_logged_in()) { wp_redirect(home_url('dashboard')); exit; }
    get_header();
    
    ?>
<!-- sidebar btns -->
<?php include_once('common/left_side_menu.php'); ?>
<!-- latest news section-->
<!-- login form -->
<div class="col-lg-6 col-md-8 padding-delete">
    <div class="mid-section login-registration-form">
        <h3 class="div-title">LOG IN</h3>
        <hr>
        <form action="<?php echo site_url('/wp-login.php');?>" method="post">
            <div class="form-group">
                <input type="text" name="log" id="user_login" class="form-control" placeholder="Email/Username" required value="">
            </div>
            <div class="form-group">
                <input type="password" name="pwd" id="user_pass" class="form-control" placeholder="Password" required>
            </div>
            <div class="custom-control custom-checkbox mb-3">
                <input type="checkbox" class="custom-control-input" id="rememberme" value="forever" name="rememberme" checked>
                <label class="custom-control-label" for="rememberme"> <span>Remember Me</span></label>
                <input type="hidden" name="redirect_to" value="<?php echo site_url( '/' ); ?>" />
                <a class="fplink" href="<?php echo site_url('/forgot-password');?>">Forgot Password</a>
            </div>
            <button type="submit" class="btn custom-btn1">Log In</button>
        </form>
        <div class="formsubmitError"><?php echo (isset($_GET['login']) && $_GET['login'] == 'failed')? 'Login Failed: Please check your login Credentials' : '';?></div>
    </div>
</div>
<!-- login form -->
<!-- app advertisement -->
<?php include_once('common/right_side_menu.php'); ?>
<!-- app advertisement -->
<?php get_footer(); ?>