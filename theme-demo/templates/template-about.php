<?php 
/* Template Name: About */
get_header();
?>
<!-- sidebar btns -->
<?php include_once('common/left_side_menu.php') ?>
<!-- latest news section-->

<div class="col-xl-6 col-lg-6 col-md-8 padding-delete">
    <div class="mid-section">
        <div class="row">
            <div class="mid-section-inner-wrapper">
                <h3 class="div-title-1"><?php the_title(); ?></h3>
                <hr>
                <?php
                // TO SHOW THE PAGE CONTENTS
                while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
                    <div class="entry-content-page">
                        <?php the_content(); ?> <!-- Page Content -->
                    </div><!-- .entry-content-page -->
                <?php
                endwhile;
                wp_reset_query();
                ?>
                <img src="assets/images/tac.png" class="img-responsive" alt="Image">
                <img src="assets/images/i-base.png" class="img-responsive" alt="Image">
                <img src="assets/images/unitaid.png" class="img-responsive" alt="Image">
                
                <div class="mid-section-btn">
                    <button class="btn custom-btn1" type="button">TAC website</button>
                    <button class="btn custom-btn1" type="button">i-Base website</button>
                    <button class="btn custom-btn1" type="button">Unitaid website</button>
                    <button class="btn custom-btn1" type="button">OK,I'm done</button>
                </div>
                
            </div>
        </div>
    </div>
</div>
<!-- app advertisement -->
<?php include_once('common/right_side_menu.php') ?>
<!-- app advertisement -->
<?php
get_footer();
?>