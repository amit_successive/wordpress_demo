<?php
add_action( 'wp_ajax_generate_notification_cron_job', 'generate_notification_cron_job' );
add_action( 'wp_ajax_nopriv_generate_notification_cron_job', 'generate_notification_cron_job' );
function generate_notification_cron_job() {
	ob_clean();
    $not = new Notification();
    $not->genAllNotification();
    echo "Generating...";
	wp_die(); // this is required to terminate immediately and return a proper response
}

add_action( 'wp_ajax_send_notification_cron_job', 'send_notification_cron_job' );
add_action( 'wp_ajax_nopriv_send_notification_cron_job', 'send_notification_cron_job' );
function send_notification_cron_job() {
	// ob_clean();
    $not = new Notification();
    $not->cronSendNotification();
    echo "Sending...";
	// wp_die(); // this is required to terminate immediately and return a proper response
}
