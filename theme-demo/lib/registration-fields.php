<?php
/**
 * FOR API & BACKEND
 */
// Registering user meta

register_meta('user', 'birth_date', array(
    'type' => 'string',
    'description' => 'user DOB',
    'single' => true,
    'show_in_rest' => true
));
register_meta('user', 'gender', array(
    'type' => 'string',
    'description' => 'gender',
    'single' => true,
    'show_in_rest' => true
));
register_meta('user', 'gender_identity', array(
    'type' => 'string',
    'description' => 'Gender identity',
    'single' => true,
    'show_in_rest' => true
));
register_meta('user', 'hiv_found_date', array(
    'type' => 'string',
    'description' => 'user DOB',
    'single' => true,
    'show_in_rest' => true
));

register_meta('user', 'art_start_date', array(
    'type' => 'string',
    'description' => 'user DOB',
    'single' => true,
    'show_in_rest' => true
));

register_meta('user', 'province_i_live', array(
    'type' => 'string',
    'description' => 'Province where I live now',
    'single' => true,
    'show_in_rest' => true
));
register_meta('user', 'city_i_live', array(
    'type' => 'string',
    'description' => 'City or town where I live now',
    'single' => true,
    'show_in_rest' => true
));
register_meta('user', 'institution_type', array(
    'type' => 'string',
    'description' => 'Where I get my ART from',
    'single' => true,
    'show_in_rest' => true
));
register_meta('user', 'institution_name', array(
    'type' => 'string',
    'description' => 'Name of clinic, hospital or pharmacy',
    'single' => true,
    'show_in_rest' => true
));
register_meta('user', 'on_medical_scheme', array(
    'type' => 'string',
    'description' => 'Medical scheme',
    'single' => true,
    'show_in_rest' => true
));
// "user/me" api update
add_filter('rest_prepare_inbox', function ($response, $data) {
    $response->data[ 'date' ] = date(DATE_NAME.' h:i a', strtotime($data->post_date_gmt));
    return $response;
}, 10, 3);

add_filter('rest_prepare_news', function ($response, $data) {
    $response->data[ 'date' ] = date(DATE_NAME.' h:i a', strtotime($data->post_date_gmt));
    return $response;
}, 10, 3);

add_filter('rest_prepare_user', function ($response, $user) {
    $response->data[ 'email' ] = $user->user_email;
    $response->data[ 'username' ] = $user->user_login;
    return $response;
}, 10, 3);


function daraptoor_rest_check_referer( $result, $server, $request ) {
    if(isset($result->data['code']) && $result->data['code'] === 'rest_user_invalid_email'){
        $result->data[ 'message' ] = 'Email already exist!';
    }
    if(isset($result->data['code']) && $result->data['code'] === 'incorrect_password'){
        return new WP_REST_Response([ 'code' => 'incorrect_password', 'message' => 'Session expired, please login again to continue.', 'code' => null ], 401 );
    }
    return $result;
}


// add the filter
add_filter( 'rest_post_dispatch', 'daraptoor_rest_check_referer', 10, 3 );

// Back-end
add_action('show_user_profile', 'crf_show_extra_profile_fields');
add_action('edit_user_profile', 'crf_show_extra_profile_fields');
function crf_show_extra_profile_fields($user) {
	?>
	<h3><?php esc_html_e('Personal Information', 'crf'); ?></h3>
	<table class="form-table">
        <tr>
			<th><label for="year_of_birth"><?php esc_html_e('FCM || DEVICE TYPE', 'crf'); ?></label></th>
			<td><?php echo esc_html(get_the_author_meta('deviceFcm', $user->ID)); ?> || <?php echo esc_html(get_the_author_meta('deviceType', $user->ID)); ?></td>
		</tr>
		<tr>
			<th><label for="year_of_birth"><?php esc_html_e('Date of birth', 'crf'); ?></label></th>
			<td><?php echo esc_html(get_the_author_meta('birth_date', $user->ID)); ?></td>
		</tr>
        <tr>
			<th><label for="year_of_birth"><?php esc_html_e('Gender', 'crf'); ?></label></th>
			<td><?php echo esc_html(get_the_author_meta('gender', $user->ID)); ?></td>
		</tr>
        <tr>
			<th><label for="year_of_birth"><?php esc_html_e('Gender Identity', 'crf'); ?></label></th>
			<td><?php echo esc_html(get_the_author_meta('gender_identity', $user->ID)); ?></td>
		</tr>
        <tr>
			<th><label for="year_of_birth"><?php esc_html_e('hiv found date', 'crf'); ?></label></th>
			<td><?php echo esc_html(get_the_author_meta('hiv_found_date', $user->ID)); ?></td>
		</tr>
        <tr>
			<th><label for="year_of_birth"><?php esc_html_e('art_start_date', 'crf'); ?></label></th>
			<td><?php echo esc_html(get_the_author_meta('art_start_date', $user->ID)); ?></td>
		</tr>
        <tr>
			<th><label for="year_of_birth"><?php esc_html_e('province_i_live', 'crf'); ?></label></th>
			<td><?php echo esc_html(get_the_author_meta('province_i_live', $user->ID)); ?></td>
		</tr>
        <tr>
			<th><label for="year_of_birth"><?php esc_html_e('city_i_live', 'crf'); ?></label></th>
			<td><?php echo esc_html(get_the_author_meta('city_i_live', $user->ID)); ?></td>
		</tr>
        <tr>
			<th><label for="year_of_birth"><?php esc_html_e('institution_type', 'crf'); ?></label></th>
			<td><?php echo esc_html(get_the_author_meta('institution_type', $user->ID)); ?></td>
		</tr>
        <tr>
			<th><label for="year_of_birth"><?php esc_html_e('institution_name', 'crf'); ?></label></th>
			<td><?php echo esc_html(get_the_author_meta('institution_name', $user->ID)); ?></td>
		</tr>
        <tr>
			<th><label for="year_of_birth"><?php esc_html_e('on_medical_scheme', 'crf'); ?></label></th>
			<td><?php echo esc_html(get_the_author_meta('on_medical_scheme', $user->ID)); ?></td>
		</tr>
	</table>
	<?php
}


/**
 * Back end display
 */

function add_column( $column ) {
    $column['device_fcm'] = 'Device Fcm';
    return $column;
}
add_filter( 'manage_users_columns', 'add_column' );

/*this will add column value in user list table*/
function add_column_value( $val, $column_name, $user_id ) {
    switch($column_name) {
        case 'device_fcm' :
            return get_user_meta($user_id, 'deviceFcm', true)." || ".get_user_meta($user_id, 'deviceType', true);
            break;

           default:
    }
}
add_filter( 'manage_users_custom_column', 'add_column_value', 10, 3 );