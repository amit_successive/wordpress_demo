<?php
class Adherence {
    private $adherenceStartDate = "adherence_start_date";
    public function addAdherence($userId, $repeted = false, $taken = 1){
        global $wpdb;
        $return = array();
        $result = $wpdb->get_results( "SELECT * FROM ".$wpdb->prefix."adherence_data WHERE user_id = $userId ORDER BY added_date DESC LIMIT 1");
        if($result){
            $date = date(DATE_NUMERIC);
            $match_date = date(DATE_NUMERIC, strtotime($result[0]->added_date));
            if($date == $match_date) {
                $streak = $result[0]->streak;
                $allowedRepet = $repeted;
            } elseif(date(DATE_NUMERIC, strtotime("-1 day")) == $match_date) {
                $streak = $result[0]->streak;
                $allowedRepet = true;
            } else {
                $streak = (int)$result[0]->streak+1;
                $allowedRepet = true;
            }
            if($allowedRepet){
                $arvPills = new ArvPills();
                $arvPill = $arvPills->getPillsCount($userId);
                if($arvPill['pillsLeft'] > 0){
                    $addToAdherence = $wpdb->insert($wpdb->prefix.'adherence_data', array(
                        'user_id' => $userId,
                        'streak' => $streak,
                        'taken' => $taken, // 1 : yes taken, 2: no
                    ));
                    if($addToAdherence){
                        $adherenceId = $wpdb->insert_id;
                        $addToPillRecord = $wpdb->insert($wpdb->prefix.'arv_pill_record', array(
                            'user_id' => $userId,
                            'adherence_id' => $adherenceId,
                            'pills_id' => $arvPill['pillId'],
                            'pills_deducted' => $arvPill['pillsITake'],
                        ));
                        if($addToPillRecord){
                            $return[SUCCESS] = 1;
                            $return[MESSAGE] = 'Data added successfully';
                        }else{
                            $return[SUCCESS] = 0;
                            $return[MESSAGE] = 'Unable to add data.';
                        }
                    }else{
                        $return[SUCCESS] = 0;
                        $return[MESSAGE] = 'undefined error';
                    }
                }else{
                    $return[SUCCESS] = 0;
                    $return[MESSAGE] = 'No pills in stock';
                }
            }else{
                $return[SUCCESS] = 0;
                $return[MESSAGE] = 'Today\'s data is already submitted';
            }
        }else{
            $arvPills = new ArvPills();
            $arvPill = $arvPills->getPillsCount($userId);
            if($arvPill['pillsLeft'] > 0){
                $addToAdherence = $wpdb->insert($wpdb->prefix.'adherence_data', array(
                    'user_id' => $userId,
                    'taken' => $taken, // 1 : yes taken, 2: no
                ));
                if($addToAdherence){
                    $adherenceId = $wpdb->insert_id;
                    $addToPillRecord = $wpdb->insert($wpdb->prefix.'arv_pill_record', array(
                        'user_id' => $userId,
                        'adherence_id' => $adherenceId,
                        'pills_id' => $arvPill['pillId'],
                        'pills_deducted' => $arvPill['pillsITake'],
                    ));
                    if($addToPillRecord){
                        $return[SUCCESS] = 1;
                        $return[MESSAGE] = 'Data added successfully';
                    }else{
                        $return[SUCCESS] = 0;
                        $return[MESSAGE] = 'Unable to add data.';
                    }
                }else{
                    $return[SUCCESS] = 0;
                    $return[MESSAGE] = 'Unable to add data!';
                }
            }else{
                $return[SUCCESS] = 0;
                $return[MESSAGE] = 'No pills in stock!';
            }
            
        }
        return $return;
    }

    public function getAdherenceHighSteak($userId) {
        global $wpdb;
        $result = $wpdb->get_results( "SELECT streak, COUNT(streak) as c FROM ".$wpdb->prefix."adherence_data WHERE user_id = $userId GROUP BY streak ORDER BY c DESC LIMIT 1");
        if($result){
            return (int)$result[0]->c;
        }else{
            return 0;
        }
    }

    public function getAdherenceCurrentSteak($userId) {
        global $wpdb;
        $adherence_start_date = get_user_meta($userId, $this->adherenceStartDate);
        if($adherence_start_date){
            $startDate = $adherence_start_date[0];
            $dateStartFormat = date("Y-m-d", strtotime($startDate));
            $result = $wpdb->get_results( "SELECT streak, COUNT(streak) as c FROM ".$wpdb->prefix."adherence_data WHERE user_id = $userId AND added_date >= '".$dateStartFormat."' GROUP BY streak ORDER BY streak DESC LIMIT 1");
            if($result){
                if(!empty(trim($result[0]->c))){
                    return (int)$result[0]->c;
                }else{
                    return 0;
                }
            }else{
                return 0;
            }
        }
    }

    public function userAdherenceConfig($userId, $startingDate, $startTime){
        $user = get_userdata( $userId );
        if ( $user === false ) {
            return 0;
        } else {
            update_user_meta($userId, $this->adherenceStartDate, $startingDate);
            update_user_meta($userId, 'adherence_notify_time', $startTime);
            return 1;
        }
       
    }

    public function getUserAdherenceConfig($userId){
        $user = get_userdata( $userId );
        if ( $user === false ) {
            return 0;
        } else {
            $util = new Utils();
            $rs['adherenceStartDate'] = $util->getUserMetaByID($userId, $this->adherenceStartDate);
            $rs['adherenceNotifyTime'] = $util->getUserMetaByID($userId, 'adherence_notify_time');
            return $rs;
        }
    }

    public function getAdherenceDaysData($userId){
        $user = get_userdata( $userId );
        $res = array();
        $totalDays = 'totalDays';
        $currentDays = 'currentDays';
        if ( $user === false ) {
            $res[$totalDays] = 0;
            $res[$currentDays] = 0;
            $res[MESSAGE] = 'undefined user';
        } else {
            global $wpdb;
            $adherence_start_date = get_user_meta($userId, $this->adherenceStartDate);
            if($adherence_start_date){
                $startDate = $adherence_start_date[0]." 00:00:00" ;
                $dateStartFormat = date("Y-m-d", strtotime($startDate));
                $endDate = date(DATE_NUMERIC);
                $diff = strtotime($endDate) - strtotime($startDate);
                $diff_in_days = floor($diff/(60*60*24));
                if($diff_in_days == 0){
                    $totalDaysc = 1;
                } elseif ($diff_in_days > 0){
                    $totalDaysc = $diff_in_days+1;
                } else {
                    $totalDaysc = 0;
                }
                $result = $wpdb->get_results( "SELECT COUNT(id) as c FROM ".$wpdb->prefix."adherence_data WHERE user_id = $userId AND added_date >= '".$dateStartFormat."' ORDER BY streak DESC LIMIT 1");
                if($result){
                    $res[$totalDays] = (int)$totalDaysc;
                    $res[$currentDays ] = (int)$result[0]->c;
                    $res[MESSAGE] = SUCCESS;
                }else{
                    $res[$totalDays] = (int)$totalDaysc;
                    $res[$currentDays ] = 1;
                    $res[MESSAGE] = SUCCESS;
                }
            }else{
                $res[$totalDays] = 0;
                $res[$currentDays ] = 0;
                $res[MESSAGE] = 'adherence start date not set';
            }
            
        }
        return $res;
    }

    public function resetAdherenceData($userId){
        global $wpdb;
        $arvPills = new ArvPills();
        $pillsData = $arvPills->getPillsCount($userId);
        $user = get_userdata( $userId );
        if ( $user === false ) {
            return 0;
        } else {
            $res = $wpdb->query(
                'DELETE FROM '.$wpdb->prefix.'adherence_data
                WHERE user_id = "'.$userId.'"'
            );
            $res2 = $wpdb->query(
                'DELETE FROM '.$wpdb->prefix.'arv_pill_record
                WHERE user_id = "'.$userId.'"'
            );
            $res3 = $wpdb->query(
                'DELETE FROM '.$wpdb->prefix.'pill_counter
                WHERE user_id = "'.$userId.'"'
            );
            if($res3){
                $arvPills->setPillsCount($userId, $pillsData['pillsLeft'], $pillsData['pillsITake'], $pillsData['remindDay'], $pillsData['remindTime']);
            }
            if($res || $res2 || $res3){
                return 1;
            }else{
                return 0;
            }
        }
    }

    public function getPercentMsg($count){
        if($count >= 95){
            return "Which is <strong>GOOD</strong>. Keep it up!";
        }elseif ($count >= 90 && $count <= 94) {
            return "Which is <strong>RISKY</strong>. Aim for at least 95%";
        }else{
            return "Which is <strong>DANGEROUS!</strong>. Aim for at least 95%";
        }
    }

}
/*
$adherence = new Adherence();
// print_r($adherence->addAdherence(1, true));
// $adherence->getAdherenceHighSteak(1);
// $adherence->getAdherenceCurrentSteak(1);
// $adherence->userAdherenceConfig(1, '07-02-2020', '10:10:11');
// print_r($adherence->getUserAdherenceConfig(1));
//print_r($adherence->getAdherenceDaysData(1));
// //$adherence->resetAdherenceData(1);
*/
?>