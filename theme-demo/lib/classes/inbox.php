<?php
class Inbox {
    public function getInboxNews($userId){
        $args = array(
            'numberposts' => 10,
            'post_type'   => 'inbox',
            'sort_order' => 'asc'
        );
        $posts = get_posts( $args );
        $res = array();
        $dataArr = array();
        $postIds = array();
        foreach ($posts as $key => $value) {
            $innerRes = array();
            $innerRes['id'] = $value->ID;
            $innerRes['date'] = date(DATE_NAME.' h:i a', strtotime($value->post_date));
            $innerRes['title']['rendered'] = $value->post_title;
            $innerRes['excerpt']['rendered'] = $value->message;
            $innerRes['external_url'] = $value->external_url;
            $innerRes['is_read'] =  $this->checkInboxRead($userId, $value->ID);
            array_push($dataArr, $innerRes);
            array_push($postIds, $value->ID);
        }
        $res['data'] = $dataArr;
        $res['postIds'] = implode(',',$postIds);
        return $res;
    }

    public function checkInboxRead($userId, $postId){
        global $wpdb;
        $result = $wpdb->get_results( "SELECT * FROM ".$wpdb->prefix."inbox_status WHERE user_id = $userId LIMIT 1");
        if($result){
            $existingData = explode(',',$result[0]->inbox_ids);
            if (in_array($postId, $existingData)) {
                $return = true;
            }else{
                $return = false;
            }
            return $return;
        }else{
            return false;
        }
    }

    public function countPendingInbox($userId){
        $args = array(
            'numberposts' => 10,
            'post_type'   => 'inbox',
            'sort_order' => 'asc'
        );
        $count = 0;
        $posts = get_posts( $args );
        if($posts){    
            foreach ($posts as $key => $value) {
                if($this->checkInboxRead($userId, $value->ID) == false) {
                    $count++;
                }
            }
        }
        return $count;
    }

    public function setInboxReadByUser($userId, $data){
        global $wpdb;
        $return = array();
        $result = $wpdb->get_results( "SELECT * FROM ".$wpdb->prefix."inbox_status WHERE user_id = $userId LIMIT 1");
        if($result){
            $dataArray = array();
            $existingData = explode(',',$result[0]->inbox_ids);
            $newData = explode(',',$data);
            $finalArray = array_unique(array_merge($existingData, $newData));
            $list = implode(',', $finalArray);
            $addToPillRecord = $wpdb->update($wpdb->prefix.'inbox_status', array(
                'inbox_ids' => $list
            ), array('user_id'=>$userId));
        }else{
            $addToPillRecord = $wpdb->insert($wpdb->prefix.'inbox_status', array(
                'user_id' => $userId,
                'inbox_ids' => $data
            ));
        }
        
        $return[SUCCESS] = 1;
        $return[MESSAGE] = 'updated successfully';
        return $return;
    }
}
// $inbox = new Inbox();
// print_r($inbox->countPendingInbox(1));