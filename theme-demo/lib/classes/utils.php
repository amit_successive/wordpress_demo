<?php

class Utils {
    private $dateFormat = "d-m-Y";
    public function dateDiffInDays($date1, $date2)  
    { 
        $diff = strtotime($date2) - strtotime($date1); 
        return abs(round($diff / 86400)); 
    }

    public function combineDateMeta($day, $month, $year, $monthDel = 'F', $seprater = "/"){
        $dayVal = ($day)? $day : date("d");
        $monthVal = ($month)? $month : date($monthDel);
        $yearVal = ($year)? $year : date("Y");
        return $dayVal.$seprater.$monthVal.$seprater.$yearVal;
    }
    
    public function getUserMeta($key){
        $userId = get_current_user_id();
        if ( metadata_exists( 'user', $userId, $key ) ) {
            return get_user_meta( $userId, $key, true );
        }else{
            return '';
        }
    }
    public function getUserMetaByID($userId, $key){
        if ( metadata_exists( 'user', $userId, $key ) ) {
            return get_user_meta( $userId, $key, true );
        }else{
            return '';
        }
    }
    public function date_range($first, $last, $step = '+1 day', $output_format = 'd/m/Y' ) {

        $dates = array();
        $current = strtotime($first);
        $last = strtotime($last);
    
        while( $current <= $last ) {
    
            $dates[] = date($output_format, $current);
            $current = strtotime($step, $current);
        }
    
        return $dates;
    }

    function validDate($date, $format = 'd-m-Y')
    {
        $d = DateTime::createFromFormat($format, trim($date));
        return $d && $d->format($format) === $date;
    }

    function validTime($time, $format = 'H:i:s')
    {
        $d = DateTime::createFromFormat($format, trim($time));
        return $d && $d->format($format) === $time;
    }

    function dateOut($date)
    {
       return (trim($date)) ? strtotime(trim($date))."000" : DDMONTHYYYY;
    }

    function timeOut($time)
    {
       return (trim($time)) ? date(TIMEOUT, strtotime(trim($time))) : "00:00 am";
    }
    
    function number_shorten($number, $precision = 3, $divisors = null) {

        // Setup default $divisors if not provided
        if (!isset($divisors)) {
            $divisors = array(
                pow(1000, 0) => '', // 1000^0 == 1
                pow(1000, 1) => 'K', // Thousand
                pow(1000, 2) => 'M', // Million
                pow(1000, 3) => 'B', // Billion
                pow(1000, 4) => 'T', // Trillion
                pow(1000, 5) => 'Qa', // Quadrillion
                pow(1000, 6) => 'Qi', // Quintillion
            );    
        }
    
        // Loop through each $divisor and find the
        // lowest amount that matches
        foreach ($divisors as $divisor => $shorthand) {
            if (abs($number) < ($divisor * 1000)) {
                // We found a match!
                break;
            }
        }
    
        // We found our match, or there were no matches.
        // Either way, use the last defined value for $divisor.
        return number_format($number / $divisor, $precision) . $shorthand;
    }
    function cron_log($log_msg, $todayDate)
    {
        $log_filename = $_SERVER['DOCUMENT_ROOT']."/log";
        if (!file_exists($log_filename)) 
        {
            // create directory/folder uploads.
            mkdir($log_filename, 0777, true);
        }
        $log_file_data = $log_filename.'/log_' . $todayDate . '.log';
        // if you don't add `FILE_APPEND`, the file will be erased each time you add a log
        file_put_contents($log_file_data, $log_msg . "\n", FILE_APPEND);
    }

    function latest_time($time = false )
    {
        $dateZoneDate = new DateTime("now", new DateTimeZone(DEFAULT_TIMEZONE));
        if($time){
            return $dateZoneDate->format(YYYYMMDD.' H:i:s');
        }else{
            return $dateZoneDate->format(YYYYMMDD);
       
        }
       
    }
}
