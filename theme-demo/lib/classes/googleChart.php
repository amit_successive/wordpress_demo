<?php 
class GoogleChart {
    public function circle($divId, $min, $max){
        echo "
        <script>
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(count_chart);
        function count_chart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Streak Days');
            data.addColumn('number', 'Total Days');
            data.addRows([
              ['', ".$min."],
              ['', ".$max."],
              
            ]);
            var options = {
                backgroundColor: 'transparent',
                width:50,
                height:50,
                chartArea:{padding:'0',width:\"100%\",height:\"100%\"},
                enableInteractivity: false,
                legend: 'none',
                pieSliceText: 'label',
                pieStartAngle: 100,
              };
            var chart = new google.visualization.PieChart(document.getElementById('".$divId."'));
            chart.draw(data, options);
          };
        </script>
        ";
    }

    public function viralLoadGraph($divId, $data){
      if($data == 0){
        echo "<script>
        document.getElementById('".$divId."').innerHTML = 'No Data to Show graph';
        </script>";
      }else{
        echo "
        <script>
        google.charts.load('current', {'packages': ['corechart','line']});
        google.charts.setOnLoadCallback(viral_load);
        function viral_load() {
          var data = google.visualization.arrayToDataTable([
            ['day', 'Viral Load'],";
            foreach ($data as $value) {
              echo "['', $value],";
            }
          echo "]);

          var options = {
            legend:'none',
            pointSize: 10,
            width:250,
            height:150,
            chartArea:{padding:\"0px\",width:\"100%\",height:\"90%\"},
            colors: ['#555'],
            vAxis:{
              baselineColor: '#fff',
              gridlineColor: '#fff',
              textPosition: 'none'
            },

            
          };

          var chart = new google.visualization.LineChart(document.getElementById('".$divId."'));

          chart.draw(data, options);
        };
        </script>
        ";
      }
    }
}

