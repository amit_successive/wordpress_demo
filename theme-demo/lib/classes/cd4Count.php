<?php
class Cd4Count {
    public function addCd4Count($userId, $cd4_count, $test_last, $test_next, $remindDaily, $remindTime){
        global $wpdb;
        $res = $wpdb->insert($wpdb->prefix.'cd4_count', array(
            'user_id' => trim($userId),
            'cd4_count' => trim($cd4_count),
            'test_last' => date(YYYYMMDD, strtotime(trim($test_last))),
            'test_next' => date(YYYYMMDD, strtotime(trim($test_next)))
        ));
        if($res){
            update_user_meta($userId, 'cd4count_next_date', trim($test_next));
            update_user_meta($userId, 'cd4count_notify_days', trim($remindDaily));
            update_user_meta($userId, 'cd4count_notify_time', trim($remindTime));
            $return = 1;
        }else{
            $return = 0;
        }
        return $return;
    }
    
    public function addNextTestDate($userId, $test_next){
        global $wpdb;
        $result = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."cd4_count WHERE user_id = $userId ORDER BY added_date DESC LIMIT 1");
        if($result){
            $wpdb->update($wpdb->prefix."cd4_count", array(
                'test_next' => date(YYYYMMDD, strtotime(trim($test_next)))
            ), array('id'=>$result[0]->id));
        }
        update_user_meta($userId, 'cd4count_next_date', trim($test_next));
        return 1;
    }

    public function getCd4Count($userId){
        global $wpdb;
        $result = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."cd4_count WHERE user_id = $userId ORDER BY added_date DESC LIMIT 1");
        
        if($result){
            $util = new Utils();
            $ret = array();
            if(!$result[0]->test_next){
                $test_next = $util->getUserMetaByID($userId, 'cd4count_next_date');
            }else{
                $test_next = $result[0]->test_next;
            }
            $ret['user_id'] = $result[0]->user_id;
            $ret['cd4_count'] = (trim($result[0]->cd4_count)) ? $result[0]->cd4_count : 0 ;
            $ret['test_last'] = date(DATE_NUMERIC, strtotime($result[0]->test_last));
            $ret['test_next'] = date(DATE_NUMERIC, strtotime($test_next));
            $ret['remindDaily'] = $util->getUserMetaByID($userId, 'cd4count_notify_days');
            $ret['remindTime'] = $util->getUserMetaByID($userId, 'cd4count_notify_time');
            $ret['added_date'] = $result[0]->added_date;
            return $ret;
        }else{
            $util = new Utils();
            $test_next = $util->getUserMetaByID($userId, 'cd4count_next_date');
            $ret = array();
            $ret['user_id'] = 0;
            $ret['cd4_count'] = '';
            $ret['test_last'] = 0;
            $ret['test_next'] = $test_next;
            $ret['remindDaily'] = 0;
            $ret['remindTime'] = 0;
            $ret['added_date'] = 0;
            return $ret;
        }
    }

    public function getGraphData($userId){
        global $wpdb;
        $result = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."cd4_count WHERE user_id = $userId ORDER BY added_date DESC LIMIT 7");
        if($result){
            $count = array();
            foreach ($result as $value) {
                array_push($count, (int)$value->cd4_count);
            }
            for ($i=1; $i < (8 - (int)count($count)); $i++) { 
                array_push($count, 0); 
            }
            return array_reverse($count);
        }else{
            return array(0 => 0, 1 => 0, 2 =>0 , 3 =>0 , 4 =>0 , 5 =>0 , 6 =>0);
        }
    }

}

// $cd4Count = new Cd4Count();
// $cd4Count->addCd4Count(1, 1500, '2020-01-30', '2020-02-05', '5', '10:10:11');
//print_r($cd4Count->getCd4Count(1));
// print_r($cd4Count->getGraphData(1));