<?php
class ArvPills {
    private $dateFormat = "Y-m-d";
    private $pill_counter = "pill_counter";
    public function __construct() {
        
    }
    public function getPillsCount($userId) {
        global $wpdb;
        $result = $wpdb->get_results( "SELECT * FROM ".$wpdb->prefix.$this->pill_counter." WHERE user_id = $userId ORDER BY added_date DESC LIMIT 1");
        if($result){
            $util = new Utils();
            $pillId = $result[0]->id;
            $pillRecord = $wpdb->get_results( "SELECT SUM(pills_deducted) as deductedPills FROM ".$wpdb->prefix."arv_pill_record WHERE user_id = $userId and pills_id = $pillId ORDER BY added_date");
            $deductedPill = $pillRecord[0]->deductedPills;
            $old_pills_count = $result[0]->old_pills_count;
            $current_pills_count = $result[0]->current_pills_count;
            $pills_i_take = $result[0]->pills_i_take;
            $start_date = $result[0]->added_date;
            $totalDays = $util->dateDiffInDays(date('d-m-Y', strtotime($start_date)), date('d-m-Y'));
            $pillsLeft = (int)(($current_pills_count+$old_pills_count)-((int)$deductedPill));
            $res['pillId'] = $pillId;
            $res['pillsITake'] = $pills_i_take;
            $res['pillsLeft'] = (int)(($pillsLeft > 0)? $pillsLeft : 0);
            $res['daysLeft'] = ((int)($pillsLeft/$pills_i_take) > 0)? (int)($pillsLeft/$pills_i_take) : 0;
            $pills_notify_days = $util->getUserMetaByID($userId, 'pills_notify_days');
            $pills_notify_time = $util->getUserMetaByID($userId, 'pills_notify_time');
            $pills_i_gets_restock = $util->getUserMetaByID($userId, 'pills_i_gets_restock');
            $pills_i_take_restock = $util->getUserMetaByID($userId, 'pills_i_take_restock');
            $res['remindDay'] = (int)(($pills_notify_days) ? $pills_notify_days : 0);
            $res['remindTime'] = ($pills_notify_time) ? $pills_notify_time : 0;
            $res['pillsIGetsRestock'] = (int)(($pills_i_gets_restock) ? $pills_i_gets_restock : 0);
            $res['pillsITakeRestock'] = (int)(($pills_i_take_restock) ? $pills_i_take_restock : 0);
            if($res['daysLeft'] < 7){
                $res['pillsMsg'] = "This is critical, I need to get more.";
            }elseif($res['daysLeft'] >= 7 && $res['daysLeft'] <=14){
                $res['pillsMsg'] = "Stock is low, plan to get more now.";
            }elseif($res['daysLeft'] > 14){
                $res['pillsMsg'] = "You are all set.";
            }else{
                $res['pillsMsg'] = "This is critical, I need to get more.";
            }
        }else{
            $res['pillId'] = 0;
            $res['pillsITake'] = 0;
            $res['pillsLeft'] = 0;
            $res['daysLeft'] = 0;
            $res['remindDay'] = 0;
            $res['remindTime'] = 0;
            $res['pillsIGetsRestock'] = 0;
            $res['pillsITakeRestock'] = 0;
            $res['pillsMsg'] = "This is critical, I need to get more.";
        }
        return $res;
    }


    public function setPillsCount($userId, $curentPills, $pillsITake, $remindDay, $remindTime) {
        global $wpdb;
        $return = array();
        $result = $wpdb->get_results( "SELECT * FROM ".$wpdb->prefix.$this->pill_counter." WHERE user_id = $userId ORDER BY added_date DESC LIMIT 1");
        if($result){
            $userData = $this->getPillsCount($userId);
            $old_pills_id = $result[0]->id;
            $old_pills_count = $userData['pillsLeft'];
            $res = $wpdb->insert($wpdb->prefix.$this->pill_counter, array(
                'user_id' => $userId,
                'old_pills_count' => (int)$old_pills_count, 
                'old_pills_id' => trim($old_pills_id),
                'current_pills_count' => trim($curentPills),
                'pills_i_take' => trim($pillsITake),
            ));
            if($res){
                //if(trim($curentPills) == 90 || trim($curentPills) == 60 || trim($curentPills) == 30){
                    update_user_meta($userId, 'pills_i_gets_restock', trim($curentPills));
                //}
                update_user_meta($userId, 'pills_i_take_restock', trim($pillsITake));
                update_user_meta($userId, 'pills_notify_days', trim($remindDay));
                update_user_meta($userId, 'pills_notify_time', trim($remindTime));
                $return[SUCCESS] = 1;
                $return[MESSAGE] = 'Data added successfully';
            }else{
                $return[SUCCESS] = 0;
                $return[MESSAGE] = 'Unable to add data.';
            }
        }else{
            $res = $wpdb->insert($wpdb->prefix.$this->pill_counter, array(
                'user_id' => $userId,
                'old_pills_count' => 0, 
                'old_pills_id' => 0, 
                'current_pills_count' => trim($curentPills), 
                'pills_i_take' => trim($pillsITake), 
            ));
            if($res){
                //if(trim($curentPills) == 90 || trim($curentPills) == 60 || trim($curentPills) == 30){
                    update_user_meta($userId, 'pills_i_gets_restock', trim($curentPills));
                //}
                update_user_meta($userId, 'pills_i_take_restock', trim($pillsITake));
                update_user_meta($userId, 'pills_notify_days', trim($remindDay));
                update_user_meta($userId, 'pills_notify_time', trim($remindTime));
                $return[SUCCESS] = 1;
                $return[MESSAGE] = 'Data added successfully';
            }else{
                $return[SUCCESS] = 0;
                $return[MESSAGE] = 'Unable to add data.';
            }
        }
        
        return $return;
    }
}
// $arvPills = new ArvPills();
// $asd = $arvPills->getPillsCount(1);
// // $asd = $arvPills->setPillsCount(1, 10, 3, 5, '10:10:10');
// print_r($asd);