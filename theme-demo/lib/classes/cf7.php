<?php
class ContactForm7Api {
    public function cf7Submit($formId , $args) {
        $url = site_url().'/wp-json/contact-form-7/v1/contact-forms/'.$formId.'/feedback';
        $response = wp_remote_post( $url, array(
            'method'      => 'POST',
            'body'        => $args,
            'cookies'     => array()
            )
        );

        if ( is_wp_error( $response ) ) {
            return $response->get_error_message();
        } else {
            if(json_decode($response['body'])->status == 'mail_sent'){
                return 'Success';
            }elseif(json_decode($response['body'])->status == 'validation_failed'){
                return 'vfail';
            }else{
                return $response['body'];
            }
            // mail_sent
        }
    }
}


