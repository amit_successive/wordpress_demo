<?php
class Notification {
    private $push_not = 'push_notification';
    private $serverKey = PUSH_NOTI_SERVER_KEY;
    public function __construct() {
        
    }

    public function saveNotification($data){
        global $wpdb;
        $saveNoty = $wpdb->insert($wpdb->prefix.$this->push_not, array(
            'user_id' => $data['userId'],
            'notify_for' => $data['noty_for'],
            'notify_title' => $data['noty_title'],
            'notify_desc' => $data['noty_desc'],
            'device_type' => $data['device_type'],
            'device_fcm' => $data['device_fcm'],
            'send_dt_stamp' => $data['send_dt_stamp'],
            'send_date' => $data['send_date'],
            'send_time' => $data['send_time'],
            'status' => '0',
            'delivered_id' => '',
        ));
        if($saveNoty){
            return '1';
        } else {
            return '0';
        }
    }

    public function checkExistNoty($userId, $tag, $date, $time){
        global $wpdb;
        $result = $wpdb->get_results( "SELECT * FROM ".$wpdb->prefix.$this->push_not." WHERE user_id = $userId and notify_for = '$tag' and send_date = '$date' and send_time = '$time' and status = '0' LIMIT 1");
        if($result){
            return 1;
        }else{
            return 2;
        }
    }

    public function removeExistNotification($userId, $tag){
        global $wpdb;
        $res = $wpdb->query(
            'DELETE FROM '.$wpdb->prefix.$this->push_not.' WHERE user_id = "'.$userId.'" AND notify_for = "'.$tag.'" AND status = "0"'
        );
        if($res){
            return 1;
        }else{
            return 2;
        }
    }

    public function removeUserAllNotification($userId){
        global $wpdb;
        $res = $wpdb->query(
            'DELETE FROM '.$wpdb->prefix.$this->push_not.' WHERE user_id = "'.$userId.'"'
        );
        if($res){
            return 1;
        }else{
            return 2;
        }
    }

    /**
    *  CD4 Notification -- Start
    */
    public function generateCD4Notification(){
        $users = get_users();
        foreach ($users as $value) {
            $userID = $value->ID;
            $this->generateSingleCD4Notification($userID);
        }
    }
    public function generateSingleCD4Notification($userId){
        $util = new Utils();
        $userID = $userId;

        $tag = CD4REMINDER;
        $title = " ";
        $desc = "ART test coming up in x days.";
        $cd4count_next_date = $util->getUserMetaByID($userID, 'cd4count_next_date');
        $cd4count_notify_days = $util->getUserMetaByID($userID, 'cd4count_notify_days');
        $cd4count_notify_time = $util->getUserMetaByID($userID, 'cd4count_notify_time');
        
        $deviceType = $util->getUserMetaByID($userID, 'deviceType');
        $deviceFcm = $util->getUserMetaByID($userID, 'deviceFcm');
        if(!empty($cd4count_next_date) && !empty($cd4count_notify_days) && !empty($cd4count_notify_time) && !empty($deviceType) && !empty($deviceFcm)){
            $startDate = date(YYYYMMDD, strtotime($cd4count_next_date . ' - '.$cd4count_notify_days.' days'));
            $endDate = date(YYYYMMDD, strtotime($cd4count_next_date));
            $time = date('H:i:s', strtotime($cd4count_notify_time));
            $dateRange = $util->date_range($startDate, $endDate, '+1 day', YYYYMMDD);

            foreach ($dateRange as $singleDate) {
                $submitData = array();
                $submitData['userId'] = $userID;
                $submitData['noty_for'] = $tag;
                $submitData['noty_title'] = $title;
                $submitData['noty_desc'] = $desc;
                $submitData['device_type'] = $deviceType;
                $submitData['device_fcm'] = $deviceFcm;
                $submitData['send_dt_stamp'] = $singleDate." ".$time;
                $submitData['send_date'] = $singleDate;
                $submitData['send_time'] = $time;
                $chk = $this->checkExistNoty($userID, $tag, $singleDate, $time);
                if($chk == 2){
                    $this->saveNotification($submitData);
                }
            }
        }
    }
    /**
    *  CD4 Notification -- END
    */
    
    /**
    *  Viral Load Notification -- START
    */

    public function generateViralLoadNotification(){
        $users = get_users();
        foreach ($users as $value) {
            $userID = $value->ID;
            $this->generateSingleViralLoadNotification($userID);
        }
    }
    public function generateSingleViralLoadNotification($userId){
        $util = new Utils();
        $userID = $userId;

        $tag = VIRALLOADREMINDER;
        $title = " ";
        $desc = "ART test coming up in x days.";
        $viralload_next_date = $util->getUserMetaByID($userID, 'viralload_next_date');
        $viralload_notify_days = $util->getUserMetaByID($userID, 'viralload_notify_days');
        $viralload_notify_time = $util->getUserMetaByID($userID, 'viralload_notify_time');
        
        $deviceType = $util->getUserMetaByID($userID, 'deviceType');
        $deviceFcm = $util->getUserMetaByID($userID, 'deviceFcm');
        if(!empty($viralload_next_date) && !empty($viralload_notify_days) && !empty($viralload_notify_time) && !empty($deviceType) && !empty($deviceFcm)){
            $startDate = date(YYYYMMDD, strtotime($viralload_next_date . ' - '.$viralload_notify_days.' days'));
            $endDate = date(YYYYMMDD, strtotime($viralload_next_date));
            $time = date('H:i:s', strtotime($viralload_notify_time));
            $dateRange = $util->date_range($startDate, $endDate, '+1 day', YYYYMMDD);

            foreach ($dateRange as $singleDate) {
                $submitData = array();
                $submitData['userId'] = $userID;
                $submitData['noty_for'] = $tag;
                $submitData['noty_title'] = $title;
                $submitData['noty_desc'] = $desc;
                $submitData['device_type'] = $deviceType;
                $submitData['device_fcm'] = $deviceFcm;
                $submitData['send_dt_stamp'] = $singleDate." ".$time;
                $submitData['send_date'] = $singleDate;
                $submitData['send_time'] = $time;
                $chk = $this->checkExistNoty($userID, $tag, $singleDate, $time);
                if($chk == 2){
                    $this->saveNotification($submitData);
                }
            }
        }
    }

    /**
    *  Viral Load Notification -- END
    */

    /**
    *  Pills Counter Notification -- START
    */

    public function generatePillCountNotification(){
        $users = get_users();
        foreach ($users as $value) {
            $userID = $value->ID;
            $this->generateSinglePillCountNotification($userID);
        }
    }
    public function generateSinglePillCountNotification($userId){
        $util = new Utils();
        $userID = $userId;
        $arvPills = new ArvPills();
        $userPillData = $arvPills->getPillsCount($userID);
        $tag = PILLREMINDER;
        $title = " ";
        $desc = "Your ART is running low – get some more.";
        $pillsDayLeft = $userPillData['daysLeft'];
        //echo "<br>";
        $remindDay = $userPillData['remindDay'];
        $remindTime = $userPillData['remindTime'];
        //echo "///<br>";
        $deviceType = $util->getUserMetaByID($userID, 'deviceType');
        $deviceFcm = $util->getUserMetaByID($userID, 'deviceFcm');
        
        if((int)($remindDay) >= $pillsDayLeft && !empty($remindDay) && !empty($remindTime) && !empty($deviceType) && !empty($deviceFcm)){
            $startDate = date(YYYYMMDD);
            $endDate = date(YYYYMMDD, strtotime('+'.$pillsDayLeft.' day'));
            $time = date('H:i:s', strtotime($remindTime));
            $dateRange = $util->date_range($startDate, $endDate, '+1 day', YYYYMMDD);
            //print_r($dateRange);
            foreach ($dateRange as $singleDate) {
                $submitData = array();
                $submitData['userId'] = $userID;
                $submitData['noty_for'] = $tag;
                $submitData['noty_title'] = $title;
                $submitData['noty_desc'] = $desc;
                $submitData['device_type'] = $deviceType;
                $submitData['device_fcm'] = $deviceFcm;
                $submitData['send_dt_stamp'] = $singleDate." ".$time;
                $submitData['send_date'] = $singleDate;
                $submitData['send_time'] = $time;
                $chk = $this->checkExistNoty($userID, $tag, $singleDate, $time);
                if($chk == 2){
                    $this->saveNotification($submitData);
                }
            }
        }
    }

    /**
    *  Pills Counter Notification -- END
    */

    /**
    *  ARV Notification -- START
    */

    public function generateARVNotification(){
        $users = get_users();
        foreach ($users as $value) {
            $userID = $value->ID;
            $this->generateSingleARVNotification($userID);
        }
    }
    public function generateSingleARVNotification($userId){
        $util = new Utils();
        $userID = $userId;
        $tag = ARVREMINDER;
        $title = " ";
        $desc = "Time to take your daily ART";
        $adherence_start_date = $util->getUserMetaByID($userID, 'adherence_start_date');
        $adherence_notify_time = $util->getUserMetaByID($userID, 'adherence_notify_time');
        
        $deviceType = $util->getUserMetaByID($userID, 'deviceType');
        $deviceFcm = $util->getUserMetaByID($userID, 'deviceFcm');
        $endDate = date(YYYYMMDD);
        if((strtotime($endDate) >= strtotime($adherence_start_date)) && !empty($adherence_start_date) && !empty($adherence_notify_time) && !empty($deviceType) && !empty($deviceFcm)){
            $time = date('H:i:s', strtotime($adherence_notify_time));
            $submitData = array();
            $submitData['userId'] = $userID;
            $submitData['noty_for'] = $tag;
            $submitData['noty_title'] = $title;
            $submitData['noty_desc'] = $desc;
            $submitData['device_type'] = $deviceType;
            $submitData['device_fcm'] = $deviceFcm;
            $submitData['send_dt_stamp'] = $endDate." ".$time;
            $submitData['send_date'] = $endDate;
            $submitData['send_time'] = $time;
            $chk = $this->checkExistNoty($userID, $tag, $endDate, $time);
            if($chk == 2){
                $this->saveNotification($submitData);
            }
        }
    }

    /**
    *  ARV Notification -- END
    */
    public function generateRemindAgainNotification($userId, $notifyOldId, $tag, $notiftTime){
        global $wpdb;
        $result = $wpdb->get_results( "SELECT * FROM ".$wpdb->prefix.$this->push_not." WHERE id = '$notifyOldId'");
        if($result){
            $oldTime = $result[0]->send_dt_stamp;
            if ($notiftTime == 'hour'){
                $endDate = $result[0]->send_date;
                $todaydate = date(YYYYMMDD.' H:i:s');
                $time = date("H:i:s", strtotime('+1 hour',strtotime($todaydate)));
                $stampTime = $endDate." ".$time;
            }elseif ($notiftTime == 'tomorrow') {
                $stampTime = date(YYYYMMDD.' H:i:s',strtotime('+1 Day',strtotime($oldTime)));
                $endDate = date(YYYYMMDD, strtotime('+1 Day',strtotime($result[0]->send_date)));
                $time = $result[0]->send_time;
            }else{
                return 0;
            }

            $submitData = array();
            $submitData['userId'] = $userId;
            $submitData['noty_for'] = $tag;
            $submitData['noty_title'] = $result[0]->notify_title;
            $submitData['noty_desc'] = $result[0]->notify_desc;
            $submitData['device_type'] = $result[0]->device_type;
            $submitData['device_fcm'] = $result[0]->device_fcm;
            $submitData['send_dt_stamp'] = $stampTime;
            $submitData['send_date'] = $endDate;
            $submitData['send_time'] = $time;
            $chk = $this->checkExistNoty($userId, $tag, $endDate, $time);
            if($chk == 2){
                $this->saveNotification($submitData);
                return 1;
            }else{
                return 2;
            }
        }else{
            return 0;
        }
    }

    public function genAllNotification(){
        $this->generateCD4Notification();
        $this->generateViralLoadNotification();
        $this->generatePillCountNotification();
        $this->generateARVNotification();
    }

    public function singleNotification($userToken, $body, $title, $notify_for, $array = array()){
        $data = array(
            'to' => $userToken,
            'collapse_key' => $notify_for,
            'notification' => array(
                "body" => $body,
                "title" => $title
            ),
            'data' => $array
        );
        $payload = json_encode($data);
        $url = "https://fcm.googleapis.com/fcm/send";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE );
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload); 
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json; charset=UTF-8","Authorization: ".$this->serverKey));
        $result = curl_exec($ch);
        $res = json_decode($result);
        curl_close($ch);
        if($res->success == 1){
            return $res->results[0]->message_id;
        }else{
            return 0;
        }
        
    }

    public function cronSendNotification(){
        $util = new Utils();
        $uniqStamp = mt_rand();
        $useCronLog = USE_CRON_LOG;
        $dateZoneDate = new DateTime("now", new DateTimeZone(DEFAULT_TIMEZONE));
        $todayDate = $dateZoneDate->format(YYYYMMDD);
        $todayStamp = $dateZoneDate->format(YYYYMMDD.' H:i:s');
        if($useCronLog){
            $util->cron_log("*****************************\n[".$todayStamp."-".$uniqStamp."]: Cron Job Started", $todayDate);
        }
        global $wpdb;
        $result = $wpdb->get_results( "SELECT * FROM ".$wpdb->prefix.$this->push_not." WHERE DATE(send_date) = '$todayDate' and send_dt_stamp <= '$todayStamp' and status = '0'");
        if($useCronLog){
            $util->cron_log("[".$util->latest_time(true)."-".$uniqStamp."]: Total Cron Item: ".count($result), $todayDate);
        }
        $successResult = 0;
        if($result){
            foreach ($result as $key => $value) {
                $notify_for = $value->notify_for;
                switch ($notify_for) {
                    case "pillReminder":
                        $arvPills = new ArvPills();
                        $datas = $arvPills->getPillsCount($value->user_id);
                        $data = $datas['daysLeft'];
                        $descc = $value->notify_desc;
                        break;
                    case "viralLoadReminder":
                        $viralLoad = new ViralLoad();
                        $datas = $viralLoad->getViralLoad($value->user_id);
                        $test_next = $datas['test_next'];
                        $data = $util->dateDiffInDays(date('d-m-Y', strtotime($test_next)), date('d-m-Y'));
                        $descc = str_replace("x", $data, $value->notify_desc);
                        if($data == 1){
                            $descc = str_replace("days", "day", $descc);
                        }
                        break;
                    case "cd4Reminder":
                        $cd4Count = new Cd4Count();
                        $datas = $cd4Count->getCd4Count($value->user_id);
                        $test_next = $datas['test_next'];
                        $data = $util->dateDiffInDays(date('d-m-Y', strtotime($test_next)), date('d-m-Y'));
                        $descc = str_replace("x", $data, $value->notify_desc);
                        if($data == 1){
                            $descc = str_replace("days", "day", $descc);
                        }
                        break;
                    default:
                        $descc = $value->notify_desc;
                        $data = '';
                }
                $arrayData = array("key" => $value->notify_for, "notifyId" => $value->id, "xData" => $data);
                if($useCronLog){
                    $util->cron_log("[".$util->latest_time(true)."-".$uniqStamp."]: Cron Id:".$value->id." Start", $todayDate);
                }
                $res = $this->singleNotification($value->device_fcm, $value->notify_title, $descc, $value->notify_for, $arrayData);
                if($useCronLog){
                    $util->cron_log("[".$util->latest_time(true)."-".$uniqStamp."]: Cron Id:".$value->id." Stop", $todayDate);
                }
                if($res){
                    if($useCronLog){
                        $util->cron_log("[".$util->latest_time(true)."-".$uniqStamp."]: Status: Success", $todayDate);
                    }
                    $addToPillRecord = $wpdb->update($wpdb->prefix.$this->push_not, array(
                        'status' => 1,
                        'delivered_id' => $res."//".serialize($arrayData)."//".date('d-m-Y H:i:s')."//".$todayStamp
                    ), array('id'=>$value->id));
                    $successResult = $successResult + 1;
                }else{
                    if($useCronLog){
                        $util->cron_log("[".$util->latest_time(true)."-".$uniqStamp."]: Status: Fail", $todayDate);
                    }
                    $addToPillRecord = $wpdb->update($wpdb->prefix.$this->push_not, array(
                        'status' => 2,
                        'delivered_id' => "//".serialize($arrayData)."//".date('Y-m-d H:i:s')."//".$todayStamp
                    ), array('id'=>$value->id));
                }
            }
        }
        if($useCronLog){
            $util->cron_log("[".$util->latest_time(true)."-".$uniqStamp."]: Total Send Success:".$successResult, $todayDate);
            $util->cron_log("[".$util->latest_time(true)."-".$uniqStamp."]: Cron Job Stopped", $todayDate);
        }
    }
}
