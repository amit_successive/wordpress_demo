<?php

class UserClass {

    public function changePassword($password, $oldPassword){
        $userId = (is_user_logged_in()) ? get_current_user_id() : 0;
        if($password){
            if($oldPassword){
                if(strlen($password) >= 6 && strlen($password) <= 15){
                    $user_data = get_userdata( $userId );
                    $check = wp_check_password( $oldPassword, $user_data->data->user_pass, $userId );
                    if ( $user_data &&  $check) {
                        wp_set_password($password, $userId);
                        $return[SUCCESS] = 1;
                        $return['token'] = "Basic ".base64_encode($user_data->data->user_login.":".$password);
                        $return[MESSAGE] = "Password updated successfully ";
                    } else {
                        $return[SUCCESS] = 0;
                        $return[MESSAGE] = "Old password is incorrect";
                    }
                }else{
                    $return[SUCCESS] = 0;
                    $return[MESSAGE] = "Password length should be min:6 & max:15";
                }
            }else{
                $return[SUCCESS] = 0;
                $return[MESSAGE] = "Old password is required";
            }
        }else{
            $return[SUCCESS] = 0;
            $return[MESSAGE] = "Password is required";
        }
        return $return;
    }

    public function changeReset($email){ 
        $pass = wp_generate_password(12, false);
        $user = get_user_by('email',$email);
        if($user){
            wp_set_password($pass, $user->ID);
            $to = $email;
            $subject = 'Modern ART for SA Reset Password';
            $body = '
                <div style="background-color:#ffffff; border:1px solid #555; text-align:center; width:600px;margin: auto">
                    <div style="height:100px;background-color:#ee3d98; display:flex; align-items:center; justify-content:center">
                        <img style="height:80px;width:100px!important;display:block;margin:auto;" src="https://modernartforsouthafrica.co.za/wp-content/themes/modern-art-successive/assets/images/logo.png"/>
                    </div>
                    <div style="padding:30px;">
                        <h3>Dear '.$user->user_nicename.'</h3>
                        <p>Your password has been reset to '.$pass.', please log in with this and change it to something you can easily remember.</p>
                        <br><p>Modern ART for SA team</p>
                        <p>'.site_url().'</p>
                    </div>
                </div>
            ';
            $headers = array('Content-Type: text/html; charset=UTF-8');
            $mail = wp_mail( $to, $subject, $body, $headers );
            if($mail){
                $return[SUCCESS] = 1;
                $return[MESSAGE] = "Password successfully reset. check your email.";
            }else{
                $return[SUCCESS] = 0;
                $return[MESSAGE] = "Unable to send mail.";
            }
        }else{
            $return[SUCCESS] = 0;
            $return[MESSAGE] = "Invalid user";
        }
        return $return;
    }

    public function getUserFCM($userId){
        $return = array();
        $utils = new Utils();
        $return['deviceType'] = $utils->getUserMetaByID($userId, 'deviceType');
        $return['deviceFcm'] = $utils->getUserMetaByID($userId, 'deviceFcm');
        return $return;
    }
}
// $userClass = new UserClass();
// print_r($userClass->getUserFCM(1));