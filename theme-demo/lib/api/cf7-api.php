<?php
/**
 * CF7 Api - START
 */

 function cf7_submit_api($data) {
        $formSubmit = new ContactForm7Api();
        $asdd = $formSubmit->cf7Submit($data['formid'], $data['feildMeta']);
        if($asdd == 'Success'){
            $res['code'] = 'Success';
            $res['message'] = "Form Submited Successfully";
        } elseif ($asdd == 'vfail') {
            return new WP_Error( 'validation_failed', 'Please check fileds data.', array( 'status' => 400 ) );
        }else{
            return new WP_Error( 'Error', 'Unable to submit form.', array( 'status' => 400 ) );
        }

return rest_ensure_response($res);
}
add_action( 'rest_api_init', function () {
 register_rest_route( 'wp/v2', '/cf7Submit/', array(
   'methods' => 'POST',
   'callback' => 'cf7_submit_api',
   'args' => array(
    'formid' => array(
        "description"=> "Please enter valid form id",
        "type"=> "string",
    ),
    'feildMeta' => array(

    ),
  ),
 ));
});