<?php
/*
* Adherence API
*/

/*
* Adherence GET API - START
*/
function user_adherence_get_api( $data ) {
    $userId = get_current_user_id();
    if ( $userId == 0 ) {
        return new WP_Error( 'undefined_user', strip_tags('Action is not allowed.'), array( 'status' => 401 ) );
    }
    $user_data = get_userdata( $userId );
    $adherence = new Adherence();
    $adherenceDays = $adherence->getAdherenceDaysData($userId);
    $res['adherenceCount'] = (int)$adherenceDays['currentDays'];
    
    $res['adherenceDays'] = $adherenceDays;
    $percentData = (int)($adherenceDays['currentDays'] != 0 )?(int)($adherenceDays['currentDays']*100/$adherenceDays['totalDays']) : 0;
    $res['adherencePercent'] = (int)$percentData;
    $res['adherenceMsg'] = $adherence->getPercentMsg($percentData);
    $res['adherenceCurrentStreak'] = (int)$adherence->getAdherenceCurrentSteak($userId);
    $res['adherenceHighStreak'] = (int)$adherence->getAdherenceHighSteak($userId);
return rest_ensure_response($res);
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'wp/v2', '/adherence/', array(
    'methods' => 'GET',
    'callback' => 'user_adherence_get_api',
    ));
} );
/*
* Adherence GET API - END
*/


/*
* Adherence POST API - START
*/
function user_adherence_post_api( $data ) {
    $userId = get_current_user_id();
    if ( $userId == 0 ) {
        return new WP_Error( 'undefined_user', strip_tags('Action is not allowed.'), array( 'status' => 401 ) );
    }
    $user_data = get_userdata( $userId );
    $adherence = new Adherence();
    $req = $adherence->addAdherence($userId, false);
    if($req[SUCCESS] == 1){
        $res[SUCCESS] = 1;
        $res[MESSAGE] = $req[MESSAGE];
    }else {
        return new WP_Error( 'error', strip_tags($req[MESSAGE]), array( 'status' => 401 ) );
    }
    
return rest_ensure_response($res);
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'wp/v2', '/adherence/', array(
    'methods' => 'POST',
    'callback' => 'user_adherence_post_api',
    ));
} );
/*
* Adherence POST API - END
*/

/*
* Adherence Setting GET API - START
*/
function user_adherence_setting_get_api( $data ) {
    $userId = get_current_user_id();
    if ( $userId == 0 ) {
        return new WP_Error( 'undefined_user', strip_tags('Action is not allowed.'), array( 'status' => 401 ) );
    }
    $adherence = new Adherence();
    $util = new Utils();
    $req = $adherence->getUserAdherenceConfig($userId);
    if($req){
        $res[SUCCESS] = 1;
        $res[MESSAGE] = "success";
        // $res['testFcmTocken'] = $util->getUserMetaByID($userId, 'testFcmTocken');
        $res['startDate'] = $util->dateOut($req['adherenceStartDate']);
        $res['reminderTime'] = $util->timeOut($req['adherenceNotifyTime']);
    }else {
        return new WP_Error( 'error', strip_tags('unable to save config data.'), array( 'status' => 406 ) );
    }

return rest_ensure_response($res);
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'wp/v2', '/adherence/setting/', array(
    'methods' => 'GET',
    'callback' => 'user_adherence_setting_get_api',
    ));
} );
/*
* Adherence Setting GET API - END
*/

/*
* Adherence Setting API - START
*/
function user_adherence_setting_put_api( $data ) {
    $userId = get_current_user_id();
    if ( $userId == 0 ) {
        return new WP_Error( 'undefined_user', strip_tags('Action is not allowed.'), array( 'status' => 401 ) );
    }
    $util = new Utils();
    if(!empty(trim($data['startDate'])) && !empty(trim($data['reminderTime'])) && $util->validDate(trim($data['startDate'])) != false && $util->validTime(trim($data['reminderTime'])) != false){
        $adherence = new Adherence();
        $startingDate = $data['startDate'];
        $startTime = $data['reminderTime'];
        // update_user_meta($userId, 'testFcmTocken', $data['testFcmTocken']);
        $req = $adherence->userAdherenceConfig($userId, $startingDate, $startTime);
        if($req){
            $not = new Notification();
            $not->removeExistNotification($userId, ARVREMINDER);
            $not->generateSingleARVNotification($userId);
            $res[SUCCESS] = 1;
            $res[MESSAGE] = "configuration saved";
            // $res['testFcmTocken'] = $util->getUserMetaByID($userId, 'testFcmTocken');
            $res['startDate'] = $util->dateOut($data['startDate']);
            $res['reminderTime'] = $util->timeOut($data['reminderTime']);
        }else {
            return new WP_Error( 'error', strip_tags('unable to save config data.'), array( 'status' => 406 ) );
        }
    }else{
        return new WP_Error( 'error', "Please send required/valid data.", array( 'status' => 401 ) );
    }
    

return rest_ensure_response($res);
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'wp/v2', '/adherence/setting/', array(
    'methods' => 'PUT',
    'callback' => 'user_adherence_setting_put_api',
    'args' => array(
        'startDate' => array(
            "description"=> "eg. 21-02-2020",
            "type"=> "string",
            "required"=> true,
        ),
        'reminderTime' => array(
            "description"=> "time for notification eg: 10:10:10",
            "type"=> "string",
            "required"=> true,
        ),
      ),
    ));
} );
/*
* Adherence Setting API - END
*/


/*
* Adherence RESET API - START
*/
function user_adherence_delete_api( $data ) {
    $userId = get_current_user_id();
    if ( $userId == 0 ) {
        return new WP_Error( 'undefined_user', strip_tags('Action is not allowed.'), array( 'status' => 401 ) );
    }
    $adherence = new Adherence();
    $req = $adherence->resetAdherenceData($userId);
    if($req){
        $res[SUCCESS] = 1;
        $res[MESSAGE] = "Adherence data reset successfully";
    }else {
        return new WP_Error( 'reset_error', strip_tags('you don\'t have any data to reset.'), array( 'status' => 406 ) );
    }
return rest_ensure_response($res);
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'wp/v2', '/adherence/', array(
    'methods' => 'DELETE',
    'callback' => 'user_adherence_delete_api',
    'description' => 'used to reset user data.'
    ));
} );
/*
* Adherence RESET API - END
*/
