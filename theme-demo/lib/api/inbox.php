<?php
/**
 * USER Dashboard - START
 */

function user_inbox_data_get_api( $data ) {

    $userId = get_current_user_id();
    if ( $userId == 0 ) {
        return new WP_Error( 'undefined_user', strip_tags('Action is not allowed.'), array( 'status' => 401 ) );
    }
    $inbox = new Inbox();
    $res = $inbox->getInboxNews($userId);
    return rest_ensure_response($res);
}
add_action( 'rest_api_init', function () {
    register_rest_route( 'wp/v2', '/inbox-data/', array(
        'methods' => 'GET',
        'callback' => 'user_inbox_data_get_api',
    ));
});

function user_inbox_data_post_api( $data ) {

    $userId = get_current_user_id();
    if ( $userId == 0 ) {
        return new WP_Error( 'undefined_user', strip_tags('Action is not allowed.'), array( 'status' => 401 ) );
    }
    $inbox = new Inbox();
    $res = $inbox->setInboxReadByUser($userId, $data['postIds']);
    return rest_ensure_response($res);
}
add_action( 'rest_api_init', function () {
    register_rest_route( 'wp/v2', '/inbox-data/', array(
        'methods' => 'POST',
        'callback' => 'user_inbox_data_post_api',
        'args' => array(
            'postIds' => array(
                "description"=> "post ids eg '1,2,3,4'",
                "type"=> "string",
                "required"=> true,
            ),
          ),
    ));
});