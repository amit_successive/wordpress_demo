<?php
/**
 * Viral Load - API
 */

/*
* Viral Load GET API - START
*/
function user_viralLoad_get_api( $data ) {
    $userId = get_current_user_id();
    if ( $userId == 0 ) {
        return new WP_Error( 'undefined_user', strip_tags('Action is not allowed.'), array( 'status' => 401 ) );
    }
    $viralLoad = new ViralLoad();
    $util = new Utils();
    $datas = $viralLoad->getViralLoad($userId);
    $res[SUCCESS] = 1;
    $res['loadCount'] = $datas['load_count'];
    $res['testLast'] = $util->dateOut($datas['test_last']);
    $res['testNext'] = $util->dateOut($datas['test_next']);
    $res['remindDaily'] = (int)$datas['remindDaily'];
    $res['remindTime'] = $util->timeOut($datas['remindTime']);
    $res['graphData'] = $viralLoad->getGraphData($userId);

return rest_ensure_response($res);
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'wp/v2', '/viral-load/', array(
    'methods' => 'GET',
    'callback' => 'user_viralLoad_get_api',
    ));
});

/*
* Viral Load GET API - END
*/
function user_viralLoad_set_api( $data ) {
    $userId = get_current_user_id();
    if ( $userId == 0 ) {
        return new WP_Error( 'undefined_user', strip_tags('Action is not allowed.'), array( 'status' => 401 ) );
    }
    $util = new Utils();
    // print_r((int)trim($data['loadCount']));
    if(!empty(trim($data['testOn'])) && !empty(trim($data['testNext'])) &&  !empty(trim($data['remindDaily'])) && !empty(trim($data['remindTime'])) && $util->validDate(trim($data['testOn'])) != false && $util->validDate(trim($data['testNext'])) != false && $util->validTime(trim($data['remindTime'])) != false){
        $viralLoad = new ViralLoad();
            $req = $viralLoad->addViralLoad(
            $userId,
            (int)trim($data['loadCount']),
            $data['testOn'],
            $data['testNext'],
            $data['remindDaily'],
            $data['remindTime']
        );
        if($req == 1){
            $not = new Notification();
            $not->removeExistNotification($userId, VIRALLOADREMINDER);
            $not->generateSingleViralLoadNotification($userId);
            $res[SUCCESS] = 1;
            $res[MESSAGE] = 'Data added successfuly.';
        }else{
            return new WP_Error( 'error', strip_tags('Unable to add data.'), array( 'status' => 400 ) );
        }
    }else{
        return new WP_Error( 'error', "Please send required data.", array( 'status' => 400 ) );
    }
    
return rest_ensure_response($res);
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'wp/v2', '/viral-load/', array(
    'methods' => 'POST',
    'callback' => 'user_viralLoad_set_api',
    'args' => array(
        'loadCount' => array(
            "description"=> "eg. 9000",
            "type"=> "string",
            "required"=> true,
        ),
        'testOn' => array(
            "description"=> "eg. 21-02-2020",
            "type"=> "string",
            "required"=> true,
        ),
        'testNext' => array(
            "description"=> "eg. 21-02-2020",
            "type"=> "string",
            "required"=> true,
        ),
        'remindDaily' => array(
            "description"=> "eg. 7, 6, 5, 4, 3, 2, 1",
            "type"=> "string",
            "required"=> true,
        ),
        'remindTime' => array(
            "description"=> "time for notification eg: 10:10:10",
            "type"=> "string",
            "required"=> true,
        ),
      ),
    ));
});
/*
* Viral Load POST API - END
*/

/*
* Viral Load GET API - END
*/
function user_viralLoad_nexttest_api( $data ) {
    $userId = get_current_user_id();
    if ( $userId == 0 ) {
        return new WP_Error( 'undefined_user', strip_tags('Action is not allowed.'), array( 'status' => 401 ) );
    }
    $util = new Utils();
    if(!empty(trim($data['testNext'])) && $util->validDate(trim($data['testNext'])) != false ){
        $viralLoad = new ViralLoad();
            $req = $viralLoad->addNextTestDate(
            $userId,
            $data['testNext']
        );
        if($req == 1){
            $not = new Notification();
            $not->removeExistNotification($userId, VIRALLOADREMINDER);
            $not->generateSingleViralLoadNotification($userId);
            $res[SUCCESS] = 1;
            $res[MESSAGE] = 'Data added successfuly.';
        }else{
            return new WP_Error( 'error', strip_tags('Unable to add data.'), array( 'status' => 400 ) );
        }
    }else{
        return new WP_Error( 'error', "Please send required data.", array( 'status' => 400 ) );
    }
    
return rest_ensure_response($res);
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'wp/v2', '/viral-load-next-test/', array(
    'methods' => 'POST',
    'callback' => 'user_viralLoad_nexttest_api',
    'args' => array(
        'testNext' => array(
            "description"=> "eg. 21-02-2020",
            "type"=> "string",
            "required"=> true,
        )
      ),
    ));
});
/*
* Viral Load POST API - END
*/