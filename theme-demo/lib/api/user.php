<?php
/**
 * USER LOGIN AUTHENTICATION - START
 */
function get_user_login( $data ) {
    $login = sanitize_text_field($data['login']);
    $password = sanitize_text_field($data['pass']);
    if (filter_var($login, FILTER_VALIDATE_EMAIL)) {
        $user = get_user_by('email', $login);
    } else {
        $user = get_user_by('login', $login);
    }
    if ($user && wp_check_password( $password, $user->data->user_pass, $user->ID)) {
        $user_login = $user->data->user_login;
        $user_pass = $password;
        update_user_meta($user->ID, 'deviceType', $data['deviceType']);
        update_user_meta($user->ID, 'deviceFcm', $data['deviceFcm']);
        $res['user_id'] = $user->ID;
        $res['user_name'] = $user->data->user_login;
        $res['user_email'] = $user->data->user_email;
        $res['token'] = "Basic ".base64_encode($user_login.":".$user_pass);
    }else{
        return new WP_Error( "invalid_cred", 'You entered wrong credentials.', array( 'status' => 401 ));
    }

  return $res;
}
add_action( 'rest_api_init', function () {
    register_rest_route( 'wp/v2', '/auth/', array(
      'methods' => 'GET',
      'callback' => 'get_user_login',
      'args' => array(
        'login' => array(
            "description"=> "username or email",
            "type"=> "string",
            "required"=> true,
        ),
        'pass' => array(
            "description"=> "user password",
            "type"=> "string",
            "required"=> true,
        ),
        'deviceType' => array(
            "description"=> "please send device type 'android' or 'ios'",
            "type"=> "string",
        ),
        'deviceFcm' => array(
            "description"=> "user fcm token",
            "type"=> "string",
        ),
      ),
    ) );
});
/**
 * USER LOGIN AUTHENTICATION - END
 */

/**
 * USER SIGNUP - START
 */
function set_user_signup( $data ) {

    $creds = array(
     'username' => $data['username'],
     'email' => $data['email'],
     'password' => $data['password'],
     'roles' => $data['roles'],
     'meta' => $data['meta'],
     );
    $user_login = sanitize_text_field( $data['username'] );
    $user_email = sanitize_email( $data['email'] );
    $user = register_new_user( $user_login, $user_email );
    if (is_wp_error( $user ) ) {
        return new WP_Error( $user->get_error_code(), strip_tags($user->get_error_message()), array( 'status' => 401 ) );
    }else{
        wp_set_password(sanitize_text_field($data['password']), $user);
        if($data['meta']){
            foreach ($data['meta'] as $key => $value) {
                update_user_meta( $user, $key, $value);
            }
        }
        $res[SUCCESS] = 1;
        $res['user_name'] = $user_login;
        $res['user_email'] = $user_email;
        $res['token'] = "Basic ".base64_encode($user_login.":".$data['password']);
        $res[MESSAGE] = "User Registered Successfully";
    }
  
   return $res;
 }
add_action( 'rest_api_init', function () {
    register_rest_route( 'wp/v2', '/signup', array(
        'methods' => 'POST',
        'callback' => 'set_user_signup',
        'args' => array(
        'username' => array(
            "description"=> "username",
            "type"=> "string",
            "required"=> true,
        ),
        'email' => array(
            "description"=> "user email",
            "type"=> "string",
            "required"=> true,
        ),
        'password' => array(
            "description"=> "user password",
            "type"=> "string",
            "required"=> true,
        ),
        'meta' => array(
           
        ),
        ),
    ));
});

/**
 * USER SIGNUP - END
 */

/**
 * USER Password Change - START
 */
function update_user_passwords( $data ) {
    $userId = get_current_user_id();
    if ( $userId == 0 ) {
        return new WP_Error( 'undefined_user', strip_tags('Action is not allowed.'), array( 'status' => 401 ) );
    }
    $userClass = new UserClass();
    $req = $userClass->changePassword($data['pass'], $data['oldpass']);
    if($req[SUCCESS] == 1){
        return $req;
    }else{
        return new WP_Error( 'error', strip_tags($req[MESSAGE]), array( 'status' => 401 ) );
    }
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'wp/v2', '/auth/password', array(
        'methods' => 'POST',
        'callback' => 'update_user_passwords',
        'args' => array(
            'pass' => array(
                "description"=> "enter new password",
                "type"=> "string",
                "required"=> true,
            ),
            'oldpass' => array(
                "description"=> "enter old password",
                "type"=> "string",
                "required"=> true,
            )
          ),
    ));
});
/**
 * USER Password Change - END
 */

 /**
 * USER Password forget - START
 */
function forget_user_passwords( $data ) {
    $email = $data['email'];
    if($email){
        $userClass = new UserClass();
        $req = $userClass->changeReset($data['email']);
        if($req[SUCCESS] == 1){
            return $req;
        }else{
            return new WP_Error( 'error', strip_tags($req[MESSAGE]), array( 'status' => 400 ) );
        }
    }else{
        return new WP_Error( 'email_not_defind', strip_tags('email is required'), array( 'status' => 401 ) );
    }
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'wp/v2', '/auth/forget', array(
        'methods' => 'POST',
        'callback' => 'forget_user_passwords',
        'args' => array(
            'email' => array(
                "description"=> "enter email",
                "type"=> "string",
                "required"=> true,
            )
          ),
    ));
});
/**
 * USER Password forget - END
 */

 /**
  * 
  */
  function get_user_logout_api( $data ) {
    $userId = get_current_user_id();
    if ( $userId == 0 ) {
        return new WP_Error( 'undefined_user', strip_tags('Action is not allowed.'), array( 'status' => 401 ) );
    }
    $not = new Notification();
    $ress = $not->removeUserAllNotification($userId);
    update_user_meta($userId, 'deviceType', '');
    update_user_meta($userId, 'deviceFcm', '');
    if($ress){
        $res[SUCCESS] = 1;
        $res[MESSAGE] = "Logged out successfully";
    }

  return $res;
}
add_action( 'rest_api_init', function () {
    register_rest_route( 'wp/v2', '/logout/', array(
      'methods' => 'POST',
      'callback' => 'get_user_logout_api',
    ) );
});