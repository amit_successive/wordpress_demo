<?php 

/*
* Adherence Setting API - START
*/

function notification_reminder_post_api( $data ) {
    $userId = get_current_user_id();
    if ( $userId == 0 ) {
        return new WP_Error( 'undefined_user', strip_tags('Action is not allowed.'), array( 'status' => 401 ) );
    }
    $util = new Utils();
    if(!empty(trim($data['notifyTag'])) && !empty(trim($data['notiftTime'])) && !empty(trim($data['notifyOldId']))){
        $not = new Notification();
        $notifyTag = $data['notifyTag'];
        $notiftTime = $data['notiftTime'];
        $notifyOldId = $data['notifyOldId'];
        $req = $not->generateRemindAgainNotification($userId, $notifyOldId, $notifyTag, $notiftTime);
        if($req == 1){
            $res[SUCCESS] = 1;
            $res[MESSAGE] = "configuration saved";
        }elseif ($req == 2) {
            $res[SUCCESS] = 1;
            $res[MESSAGE] = "configuration saved.";
        }
        else {
            return new WP_Error( 'error', strip_tags('unable to save notification data.'), array( 'status' => 406 ) );
        }
    }else{
        return new WP_Error( 'error', "Please send required/valid data.", array( 'status' => 401 ) );
    }
    

return rest_ensure_response($res);
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'wp/v2', '/notification-reminder/', array(
    'methods' => 'POST',
    'callback' => 'notification_reminder_post_api',
    'args' => array(
        'notifyOldId' => array(
            "description"=> "notification Id",
            "type"=> "string",
            "required"=> true,
        ),
        'notifyTag' => array(
            "description"=> "eg. 'arvReminder','pillReminder','viralLoadReminder','cd4Reminder'",
            "type"=> "string",
            "required"=> true,
        ),
        'notiftTime' => array(
            "description"=> "time for notification eg: 'hour','tomorrow'",
            "type"=> "string",
            "required"=> true,
        ),
      ),
    ));
} );
/*
* Adherence Setting API - END
*/