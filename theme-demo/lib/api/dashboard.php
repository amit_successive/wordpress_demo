<?php
/**
 * USER Dashboard - START
 */

 function user_dashboard_api( $data ) {

    $userId = get_current_user_id();
    if ( $userId == 0 ) {
        return new WP_Error( 'undefined_user', strip_tags('Action is not allowed.'), array( 'status' => 401 ) );
    }
    $user_data = get_userdata( $userId );
    $adherence = new Adherence();
    $arvPills = new ArvPills();
    $pillsData = $arvPills->getPillsCount($userId);
    $viralLoad = new ViralLoad();
    $cd4Count = new Cd4Count();
    $utils = new Utils();
    $inbox = new Inbox();
    $res['userImage'] = '';
    $res['name'] = $user_data->user_login;
    $res['artSince'] = ($utils->getUserMeta('art_start_date') == "Invalid date" || empty($utils->getUserMeta('art_start_date')) ? null : date( DATE_NAME, strtotime($utils->getUserMeta('art_start_date'))));
    $adherenceDays = $adherence->getAdherenceDaysData($userId);
    $res['adherenceCount'] = (int)$adherenceDays['currentDays'];
    $res['pillCountDays'] = (int)$pillsData['daysLeft'];
    $res['inboxCount'] = $inbox->countPendingInbox($userId);
    $res['myViralLoadCount'] = (int)$viralLoad->getViralLoad($userId)['load_count'];
    $res['latestTest'] = ($viralLoad->getViralLoad($userId)['test_last'] != 0) ? date_format(date_create($viralLoad->getViralLoad($userId)['test_last']), DATE_NAME) : DDMONTHYYYY;
    $res['nextTest'] = ($viralLoad->getViralLoad($userId)['test_next'] != 0) ? date_format(date_create($viralLoad->getViralLoad($userId)['test_next']), DATE_NAME) : DDMONTHYYYY;
    $res['cd4Count'] = (int)$cd4Count->getCd4Count($userId)['cd4_count'];
    $res['cd4latestTest'] = ($cd4Count->getCd4Count($userId)['test_last'] != 0) ? date_format(date_create($cd4Count->getCd4Count($userId)['test_last']), DATE_NAME) : DDMONTHYYYY;
    $res['cd4nextTest'] =($cd4Count->getCd4Count($userId)['test_next'] != 0) ? date_format(date_create($cd4Count->getCd4Count($userId)['test_next']), DATE_NAME) : DDMONTHYYYY;
return rest_ensure_response($res);
}
add_action( 'rest_api_init', function () {
 register_rest_route( 'wp/v2', '/dashboard/', array(
   'methods' => 'GET',
   'callback' => 'user_dashboard_api',
 ));
} );