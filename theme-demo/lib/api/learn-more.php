<?php
/*
* Learn More API
*/

/*
*  Learn More Menu API - START
*/

function learn_more_menu_get_api() {
    $menu_name = 'forth';
    $locations = get_nav_menu_locations();
    $menu = wp_get_nav_menu_object($locations[$menu_name]);
    $menuitems = wp_get_nav_menu_items( $menu->term_id, array( 'order' => 'DESC' ) );
    $res = array();
    foreach ( $menuitems as $item ) {
        $resDetail = array();
        $resDetail['ID'] = $item->ID;
        $resDetail['title'] = htmlspecialchars_decode($item->title);
        $resDetail['subTitle'] = get_field('sub_menu_feild', $item);
        $resDetail['postName'] = $item->post_name;
        $resDetail['isEnable'] = (get_field('enable_menu_item', $item) == 'yes') ? true : false;
        $resDetail['pageViewType'] = $item->object;
        if($item->object == 'page'){
            $pageID = (int)$item->object_id;
            $resDetail['pageID'] = $pageID;
            $resDetail['mainContent'] = get_field( "content_for_landing_page", $pageID );
            $resDetail['topImage'] = get_field( "top_image", $pageID );
            $resDetail['bottomImage'] = get_field( "bottom_image", $pageID );
            $resDetail['buttonText'] = get_field( "button_text", $pageID );
            $resDetail['pageType'] = get_field( "page_type", $pageID );
        }else{
            $resDetail['pageID'] =  $item->url;
            $resDetail['mainContent'] = '';
            $resDetail['topImage'] = '';
            $resDetail['bottomImage'] = '';
            $resDetail['buttonText'] = '';
            $resDetail['pageType'] = '';
        }
        
        array_push($res, $resDetail);
    }
    return rest_ensure_response($res);
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'wp/v2', '/learn-more/menu', array(
    'methods' => 'GET',
    'callback' => 'learn_more_menu_get_api',
    ));
} );
/*
* Learn More Menu - END
*/
function get_pageId_by_key($check){
    $jsonData = learn_more_menu_get_api();
    $response = array();
    foreach ($jsonData->data as $value) {
        if($value['pageType'] == $check){
            $response['pageID'] = $value['pageID'];
            $response['mainContent'] = $value['mainContent'];
            $response['topImage'] = $value['topImage'];
            $response['bottomImage'] = $value['bottomImage'];
            $response['buttonText'] = $value['buttonText'];
            $response['pageType'] = $value['pageType'];
        }
    }
    return $response;
}

/*
*  Learn More Page API - START
*/
function learn_more_single_page_get_api(WP_REST_Request $data ) {
    $pageId = sanitize_text_field( $data->get_param('id') );
    $page_content = get_post($pageId);
    $res['pageTitle'] = $page_content->post_title;
    $ads = get_field('accordion_section', $pageId);
    $count = 0;
    $metaArr = array();
    if($ads){
        foreach ($ads as $key => $value){
            $meta = array();
            $contentArr = array();
            $meta['title'] = strtoupper($value['accordian_title']);
            if($value['above_title_image']){
                $meta['aboveTitleImage'] = $value['above_title_image'];
                $meta['aboveTitleImageSize'] = $value['above_title_image_size'];
            }
            if(in_array('Mobile', $value['visibility'])){
                $count++;
                foreach ($value['accordian_detail_wrapper'] as $key2 => $value2) {
                    $meta2 = array();
                    $meta2['content'] = $value2['accordian_detail'];
                    if($value2['accordian_image']){
                        $meta2['image'] = $value2['accordian_image'];
                        $meta2['imagePosition'] = ($value2['image_position']) ? $value2['image_position'] : 'Bottom';
                        $meta2['imageSize'] = ($value2['image_size']) ? $value2['image_size'] : 'medium';
                    }
                    if($value2['navigation_button_text']){
                        $meta2['navigation_button_text'] = ($value2['navigation_button_text']) ? $value2['navigation_button_text'] : '';
                        $meta2['navigate_page_for_button'] = ($value2['navigate_page_for_button']) ? $value2['navigate_page_for_button'] : '';
                        $meta2['navigation_data'] = ($value2['navigate_page_for_button']) ? get_pageId_by_key($value2['navigate_page_for_button']) : '';
                    }
                    
                    array_push($contentArr, $meta2);
                }
                $meta['contentDetail'] = $contentArr;
                array_push($metaArr, $meta);
            }
        }
    }
    $res['totalCount'] = $count;
    $res['meta'] = $metaArr;
    return rest_ensure_response($res);
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'wp/v2', '/learn-more/page/(?P<id>[\d]+)', array(
    'methods' => 'GET',
    'callback' => 'learn_more_single_page_get_api',
    'args' => array(
        'id' => array(
            "description"=> "page ID",
            "type"=> "string",
            "required"=> true,
        ),
      ),
    ));
} );
/*
* Learn More Page - END
*/