<?php
/*
* Pills API
*/

/*
* Pills GET API - START
*/
function user_pills_get_api( $data ) {
    $userId = get_current_user_id();
    if ( $userId == 0 ) {
        return new WP_Error( 'undefined_user', strip_tags('Action is not allowed.'), array( 'status' => 401 ) );
    }
    $arvPills = new ArvPills();
    $util = new Utils();
    $datas = $arvPills->getPillsCount($userId);
    
    $res[SUCCESS] = 1;
    $res['daysLeft'] = $datas['daysLeft'];
    $res['pillsMsg'] = $datas['pillsMsg'];
    $res['pillsLeft'] = $datas['pillsLeft'];
    $res['pillsIGetsRestock'] = $datas['pillsIGetsRestock'];
    $res['pillsITakeRestock'] = $datas['pillsITakeRestock'];
    $res['remindDay'] = $datas['remindDay'];
    $res['remindTime'] = $util->timeOut($datas['remindTime']);

return rest_ensure_response($res);
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'wp/v2', '/pills/', array(
    'methods' => 'GET',
    'callback' => 'user_pills_get_api',
    ));
} );
/*
* Pills GET API - END
*/

/*
* Pills SET API - START
*/
function user_pills_set_api( $data ) {
    $userId = get_current_user_id();
    if ( $userId == 0 ) {
        return new WP_Error( 'undefined_user', strip_tags('Action is not allowed.'), array( 'status' => 401 ) );
    }
    $util = new Utils();
    if(!empty(trim($data['curentPills'])) && !empty(trim($data['pillsITake'])) &&  !empty(trim($data['remindDay'])) && !empty(trim($data['remindTime'])) && $util->validTime(trim($data['remindTime'])) != false){
        $arvPills = new ArvPills();
        $req = $arvPills->setPillsCount($userId, $data['curentPills'], $data['pillsITake'], $data['remindDay'], $data['remindTime']);
        if($req[SUCCESS] == 1){
            $not = new Notification();
            $not->removeExistNotification($userId, PILLREMINDER);
            $not->generateSinglePillCountNotification($userId);
            $res = $req;
        }else{
            return new WP_Error( 'error', strip_tags($req['message']), array( 'status' => 400 ) );
        }
    }else{
        return new WP_Error( 'error', "Please send required/valid data.", array( 'status' => 400 ) );
    }
    
return rest_ensure_response($res);
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'wp/v2', '/pills/', array(
    'methods' => 'POST',
    'callback' => 'user_pills_set_api',
    'args' => array(
        'curentPills' => array(
            "description"=> "eg. 90, 60, 30",
            "type"=> "string",
            "required"=> true,
        ),
        'pillsITake' => array(
            "description"=> "eg. 3, 2, 1",
            "type"=> "string",
            "required"=> true,
        ),
        'remindDay' => array(
            "description"=> "eg. 7, 6, 5, 4, 3, 2, 1",
            "type"=> "string",
            "required"=> true,
        ),
        'remindTime' => array(
            "description"=> "time for notification eg: 10:10:10",
            "type"=> "string",
            "required"=> true,
        ),
      ),
    ));
});
/*
* Pills SET API - END
*/