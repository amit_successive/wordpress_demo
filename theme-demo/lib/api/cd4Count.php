<?php
/**
 * CD4 Count - API
 */

/*
* CD4 Count GET API - START
*/
function user_cd4_get_api( $data ) {
    $userId = get_current_user_id();
    if ( $userId == 0 ) {
        return new WP_Error( 'undefined_user', strip_tags('Action is not allowed.'), array( 'status' => 401 ) );
    }
    $cd4Count = new Cd4Count();
    $util = new Utils();
    $datas = $cd4Count->getCd4Count($userId);
    $res[SUCCESS] = 1;
    $res['cd4Count'] = $datas['cd4_count'];
    $res['testLast'] = $util->dateOut($datas['test_last']);
    $res['testNext'] = $util->dateOut($datas['test_next']);
    $res['remindDaily'] = (int)$datas['remindDaily'];
    $res['remindTime'] = $util->timeOut($datas['remindTime']);
    $res['graphData'] = $cd4Count->getGraphData($userId);

return rest_ensure_response($res);
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'wp/v2', '/cd4/', array(
    'methods' => 'GET',
    'callback' => 'user_cd4_get_api',
    ));
});

/*
* CD4 Count GET API - END
*/
function user_cd4_set_api( $data ) {
    $userId = get_current_user_id();
    if ( $userId == 0 ) {
        return new WP_Error( 'undefined_user', strip_tags('Action is not allowed.'), array( 'status' => 401 ) );
    }
    $util = new Utils();
    if(!empty(trim($data['testOn'])) && !empty(trim($data['testNext'])) &&  !empty(trim($data['remindDaily'])) && !empty(trim($data['remindTime'])) && $util->validDate(trim($data['testOn'])) != false && $util->validDate(trim($data['testNext'])) != false && $util->validTime(trim($data['remindTime'])) != false){
        $cd4Count = new Cd4Count();
            $req = $cd4Count->addCd4Count(
            $userId,
            (int)trim($data['cd4Count']),
            $data['testOn'],
            $data['testNext'],
            $data['remindDaily'],
            $data['remindTime']
        );
        if($req == 1){
            $not = new Notification();
            $not->removeExistNotification($userId, CD4REMINDER);
            $not->generateSingleCD4Notification($userId);
            $res[SUCCESS] = 1;
            $res[MESSAGE] = 'Data added successfuly.';
        }else{
            return new WP_Error( 'error', strip_tags('Unable to add data.'), array( 'status' => 400 ) );
        }
    }else{
        return new WP_Error( 'error', "Please send required/valid data.", array( 'status' => 400 ) );
    }
    
return rest_ensure_response($res);
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'wp/v2', '/cd4/', array(
    'methods' => 'POST',
    'callback' => 'user_cd4_set_api',
    'args' => array(
        'cd4Count' => array(
            "description"=> "eg. 9000",
            "type"=> "string",
            "required"=> true,
        ),
        'testOn' => array(
            "description"=> "eg. 21-02-2020",
            "type"=> "string",
            "required"=> true,
        ),
        'testNext' => array(
            "description"=> "eg. 21-02-2020",
            "type"=> "string",
            "required"=> true,
        ),
        'remindDaily' => array(
            "description"=> "eg. 7, 6, 5, 4, 3, 2, 1",
            "type"=> "string",
            "required"=> true,
        ),
        'remindTime' => array(
            "description"=> "time for notification eg: 10:10:10",
            "type"=> "string",
            "required"=> true,
        ),
      ),
    ));
});
/*
* CD4 Count POST API - END
*/


/*
* CD4 Count GET API - END
*/
function user_cd4_nexttest_api( $data ) {
    $userId = get_current_user_id();
    if ( $userId == 0 ) {
        return new WP_Error( 'undefined_user', strip_tags('Action is not allowed.'), array( 'status' => 401 ) );
    }
    $util = new Utils();
    if(!empty(trim($data['testNext'])) && $util->validDate(trim($data['testNext'])) != false){
        $cd4Count = new Cd4Count();
            $req = $cd4Count->addNextTestDate(
            $userId,
            $data['testNext']
        );
        if($req == 1){
            $not = new Notification();
            $not->removeExistNotification($userId, CD4REMINDER);
            $not->generateSingleCD4Notification($userId);
            $res[SUCCESS] = 1;
            $res[MESSAGE] = 'Data added successfuly.';
        }else{
            return new WP_Error( 'error', strip_tags('Unable to add data.'), array( 'status' => 400 ) );
        }
    }else{
        return new WP_Error( 'error', "Please send required/valid data.", array( 'status' => 400 ) );
    }
    
return rest_ensure_response($res);
}

add_action( 'rest_api_init', function () {
    register_rest_route( 'wp/v2', '/cd4-next-test/', array(
    'methods' => 'POST',
    'callback' => 'user_cd4_nexttest_api',
    'args' => array(
        'testNext' => array(
            "description"=> "eg. 21-02-2020",
            "type"=> "string",
            "required"=> true,
        )
      ),
    ));
});
/*
* CD4 Count POST API - END
*/