<?php
global $wpdb;
/*
* Adherence Table - Start 
*/
$table_adherence = $wpdb->prefix . "adherence_data";
$charset_collate = $wpdb->get_charset_collate();

if ( $wpdb->get_var( "SHOW TABLES LIKE '{$table_adherence}'" ) != $table_adherence ) {

    $sql = "CREATE TABLE $table_adherence (
        `id` BIGINT(9) NOT NULL AUTO_INCREMENT ,
        `user_id` BIGINT(9) NOT NULL ,
        `taken` INT(1) NOT NULL DEFAULT '1',
        `streak` INT(11) NOT NULL DEFAULT '1',
        `added_date` datetime NULL default CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)
    )$charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
}
/*
* Adherence Table - End 
*/

/*
* Pill Counter Table - Start 
*/
$pill_counter = $wpdb->prefix . "pill_counter";
$charset_collate = $wpdb->get_charset_collate();

if ( $wpdb->get_var( "SHOW TABLES LIKE '{$pill_counter}'" ) != $pill_counter ) {
    $sql = "CREATE TABLE $pill_counter (
        `id` BIGINT(9) NOT NULL AUTO_INCREMENT ,
        `user_id` BIGINT(9) NOT NULL ,
        `old_pills_count` INT(11) NOT NULL DEFAULT '0',
        `old_pills_id` INT(11) NOT NULL DEFAULT '0',
        `current_pills_count` INT(11) NOT NULL DEFAULT '0',
        `pills_i_take` INT(11) NOT NULL DEFAULT '0',
        `added_date` datetime NULL default CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)
    )$charset_collate;";
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
}
/*
* Pill Counter Table - End 
*/

/*
* Pill Counter Table - Start 
*/
$arv_pill_record = $wpdb->prefix . "arv_pill_record";
$charset_collate = $wpdb->get_charset_collate();

if ( $wpdb->get_var( "SHOW TABLES LIKE '{$arv_pill_record}'" ) != $arv_pill_record ) {
    $sql = "CREATE TABLE $arv_pill_record (
        `id` BIGINT(9) NOT NULL AUTO_INCREMENT ,
        `user_id` BIGINT(9) NOT NULL,
        `adherence_id` INT(11) NOT NULL,
        `pills_id` INT(11) NOT NULL,
        `pills_deducted` INT(11) NOT NULL,
        `taken_on` datetime NULL DEFAULT CURRENT_TIMESTAMP,
        `added_date` datetime NULL default CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)
    )$charset_collate;";
    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
}
/*
* Pill Counter Table - End 
*/

/*
* Viral Load Table - Start 
*/
$table_viral_load = $wpdb->prefix . "viral_load";
$charset_collate = $wpdb->get_charset_collate();

if ( $wpdb->get_var( "SHOW TABLES LIKE '{$table_viral_load}'" ) != $table_viral_load ) {

    $sql = "CREATE TABLE $table_viral_load (
        `id` BIGINT(9) NOT NULL AUTO_INCREMENT ,
        `user_id` BIGINT(9) NOT NULL ,
        `load_count` VARCHAR(255) NULL ,
        `test_last` date NULL ,
        `test_next` date NULL ,
        `added_date` datetime NULL default CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)
    )$charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
}
/*
* Viral Load Table - End 
*/
/*
* cd4_count Table - Start 
*/
$cd4_count = $wpdb->prefix . "cd4_count";
$charset_collate = $wpdb->get_charset_collate();

if ( $wpdb->get_var( "SHOW TABLES LIKE '{$cd4_count}'" ) != $cd4_count ) {

    $sql = "CREATE TABLE $cd4_count (
        `id` BIGINT(9) NOT NULL AUTO_INCREMENT ,
        `user_id` BIGINT(9) NOT NULL ,
        `cd4_count` VARCHAR(255) NULL ,
        `test_last` date NULL ,
        `test_next` date NULL ,
        `added_date` datetime NULL default CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)
    )$charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
}
/*
* cd4_count Table - End 
*/

/**
 * Push Notification save detail - Start
 */

$push_notification = $wpdb->prefix . "push_notification";
$charset_collate = $wpdb->get_charset_collate();
if ( $wpdb->get_var( "SHOW TABLES LIKE '{$push_notification}'" ) != $push_notification ) {
    $sql = "CREATE TABLE $push_notification (
        `id` BIGINT(9) NOT NULL AUTO_INCREMENT ,
        `user_id` BIGINT(9) NOT NULL ,
        `notify_for`  TEXT NULL ,
        `notify_title` VARCHAR(255) NULL ,
        `notify_desc` VARCHAR(255) NULL ,
        `device_type` ENUM('android','ios') NOT NULL ,
        `device_fcm` VARCHAR(255) NULL ,
        `send_dt_stamp` datetime NOT NULL ,
        `send_date` DATE NOT NULL ,
        `send_time` TIME NOT NULL ,
        `status` ENUM('0','1','2') NOT NULL DEFAULT '0' ,
        `delivered_id` VARCHAR(255) NULL ,
        `added_date` datetime NULL default CURRENT_TIMESTAMP,
        `updated_date` datetime on update CURRENT_TIMESTAMP NOT NULL default CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)
    )$charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
}

/**
 * Push Notification save detail - End
 */

 /**
 * Push Notification save detail - Start
 */

$inbox_status = $wpdb->prefix . "inbox_status";
$charset_collate = $wpdb->get_charset_collate();
if ( $wpdb->get_var( "SHOW TABLES LIKE '{$inbox_status}'" ) != $inbox_status ) {
    $sql = "CREATE TABLE $inbox_status (
        `id` BIGINT(9) NOT NULL AUTO_INCREMENT ,
        `user_id` BIGINT(9) NOT NULL ,
        `inbox_ids` TEXT NULL ,
        `added_date` datetime NULL default CURRENT_TIMESTAMP,
        `updated_date` datetime on update CURRENT_TIMESTAMP NOT NULL default CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`)
    )$charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
}

/**
 * Push Notification save detail - End
 */