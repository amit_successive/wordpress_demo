<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package modern_art_successive
 */
?>
</main>
</div><!-- #content -->

<footer id="colophon" class="site-footer footer">
    <div class="row">
      <?php
      global $post;
       if(get_field('is_footer_visible', $post->ID) == 1){?>
        <figure>
          <img src="<?php echo TEMPLATE_URL; ?>/assets/images/footer-banner.png" class="img-responsive" alt="image">
        </figure>
        <style>
            .bg-gray{
              padding-bottom:100px!important;
            }
            .tab-content.mid-section-inner-wrapper {
                padding-bottom: 100px;
            }
          </style>
        <?php } ?>
    </div>
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer();?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fingerprintjs2/2.1.0/fingerprint2.min.js" integrity="sha512-n9OoMZw+LpkURqH5ODKhxjs08ZI97Pi4pIxEnqz2mHhzZmjW7UeOm/0+lvSjQ6lepdpGkPA7L8chC0UtUbXxZg==" crossorigin="anonymous"></script>
<script src="<?php echo TEMPLATE_URL; ?>/assets/js/popper.min.js"></script>
<script src="<?php echo TEMPLATE_URL; ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php echo TEMPLATE_URL; ?>/assets/js/datepicker.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="<?php echo TEMPLATE_URL; ?>/assets/js/custom.js"></script>
</body>

</html>