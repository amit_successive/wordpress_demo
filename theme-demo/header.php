<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package modern_art_successive
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<!-- Custom files - Start-->
	<link rel="stylesheet" href="<?php echo TEMPLATE_URL;?>/assets/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
	<!-- fonts -->
    <link rel="stylesheet" href="https://use.typekit.net/yhn1whs.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&display=swap" rel="stylesheet">
	<!-- fonts -->
	<!-- datepicker css -->
	<link rel="stylesheet" href="<?php echo TEMPLATE_URL;?>/assets/css/datepicker.css" type="text/css">
	<link rel="stylesheet" href="<?php echo TEMPLATE_URL;?>/assets/css/theme.css?v=0.<?php echo time();?>" type="text/css">
	<link rel="stylesheet" href="<?php echo TEMPLATE_URL;?>/assets/css/responsive.css?v=0.<?php echo time();?>" type="text/css">
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<!-- Custom files - End-->
	<link rel="shortcut icon" type="image/png" href="<?php echo TEMPLATE_URL;?>/assets/images/logo.png" type="image/png"/>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<header id="masthead" class="site-header header">
	<div class="row">
                <div class="col-md-3 col-sm-4 col-6">
                    <div id="mySidenav" class="sidenav">
                        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
						<?php 
							if (is_user_logged_in()) { ?>
								<?php
									$locations = get_nav_menu_locations();
									$menu = wp_get_nav_menu_object($locations['menu-1']);
									$menuitems = wp_get_nav_menu_items( $menu->term_id, array( 'order' => 'DESC' ) );
									foreach ( $menuitems as $item ):
										$id = get_post_meta( $item->ID, '_menu_item_object_id', true );
										$page = get_page( $id );
										$link = get_page_link( $id );
								?>
						
							<a href="<?php echo $item->url; ?>">
								<?php echo $page->post_title; ?><br>
								<span><?php the_field('sub_menu_feild', $item);?></span>
							</a>
							<?php endforeach; ?>
						<?php }else{?>
							<?php
									$locations = get_nav_menu_locations();
									$menu = wp_get_nav_menu_object($locations['third']);
									$menuitems = wp_get_nav_menu_items( $menu->term_id, array( 'order' => 'DESC' ) );
									foreach ( $menuitems as $item ):
										$id = get_post_meta( $item->ID, '_menu_item_object_id', true );
										$page = get_page( $id );
										$link = get_page_link( $id );
								?>
						
							<a href="<?php echo $item->url; ?>">
								<?php echo $page->post_title; ?><br>
								<span><?php the_field('sub_menu_feild', $item);?></span>
							</a>
							<?php endforeach; ?>
						<?php }?>
                    </div>
                    <span id="menu" style="font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span>
					<a href="<?php echo site_url(); ?>">
						<img src="<?php echo TEMPLATE_URL;?>/assets/images/logo.png" class="navbar-brand img-responsive" alt="Logo">
					</a>
                </div>
                <div class="col-md-3 col-sm-4 col-6">
                    <div class="header-text">
                        <p>Everything you need to know about antiretroviral treatment (ART or ARVs) in South Africa.</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4 col-6">
                    <div class="brand-icon">
						<a href="https://tac.org.za/" target="_blank">
							<img src="<?php echo TEMPLATE_URL;?>/assets/images/brand-icon-01.png" class="img-responsive header-img1" alt="image">
						</a>
						<a href=" http://i-base.info/" target="_blank">
						<img src="<?php echo TEMPLATE_URL;?>/assets/images/brand-icon-02.png" class="img-responsive header-img2" alt="image">
						</a>
                        <a href="https://unitaid.org/" target="_blank"> 
							<img src="<?php echo TEMPLATE_URL;?>/assets/images/brand-icon-03.png" class="img-responsive header-img3" alt="image">
						</a>
                        
                    </div>
                </div>

                <div class="col-md-3 col-sm-4 col-6">
				<?php 
				if (!is_user_logged_in()) { ?>
					<div class="login-register">
                        <a href="<?php echo site_url('login'); ?>"><button class="btn custom-button">Login</button></a>
                        <a href="<?php echo site_url('register'); ?>"><button class="btn custom-button">Register</button></a>
                    </div>
				<?php } else { ?>
					<div class="login-register">
                        <a href="<?php echo site_url('dashboard'); ?>">
						<button class="btn custom-button">
						<img src="https://modernartforsouthafrica.co.za/wp-content/uploads/2020/05/dashboard.png" alt="my dashboard" style="    height: 20px;
						padding: 0 5px 4px 0;
						margin-left: -3px;">My dashboard</button>
						</a>
                    </div>
				<?php }
				?>
                </div>
            </div>
		
	</header><!-- #masthead -->
	<main>
	<div class="row">