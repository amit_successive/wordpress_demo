<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package modern_art_successive
 */

get_header();
if(get_post_type() == 'news'){
	include_once(ABSPATH."wp-content/themes/modern-art-successive/templates/common/left_side_menu.php");
	get_template_part( 'template-parts/content', get_post_type() );
}else{
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
		<?php
		while ( have_posts() ) :
			the_post();

			get_template_part( 'template-parts/content', get_post_type() );

			the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
}
if(get_post_type() == 'news'){
	include_once(ABSPATH.'wp-content/themes/modern-art-successive/templates/common/right_side_menu.php');
}else{
	get_sidebar();
}

get_footer();
