<?php
/**
 * modern art successive functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package modern_art_successive
 */
define('TEMPLATE_URL', get_template_directory_uri());
define('DATE_NUMERIC', "d-m-Y");
define('YYYYMMDD', "Y-m-d");
define('DATE_NAME', "d F Y");
define('DDMONTHYYYY', "dd month yyyy");
define('TIMEOUT', 'h:i a');
define('SUCCESS', "success");
define('MESSAGE', "message");
define('CD4REMINDER', "cd4Reminder");
define('VIRALLOADREMINDER', "viralLoadReminder");
define('PILLREMINDER', "pillReminder");
define('ARVREMINDER', "arvReminder");
define('PUSH_NOTI_SERVER_KEY', "--------");
define('DEFAULT_TIMEZONE', 'Africa/Johannesburg');
define('USE_CRON_LOG', true);
date_default_timezone_set(DEFAULT_TIMEZONE);

if (!function_exists('modern_art_successive_setup')):
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function modern_art_successive_setup()
{
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on modern art successive, use a find and replace
         * to change 'modern-art-successive' to the name of your theme in all the template files.
         */
        load_theme_textdomain('modern-art-successive', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'menu-1' => esc_html__('Primary', 'modern-art-successive'),
        ));
        
        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('modern_art_successive_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support('custom-logo', array(
            'height' => 250,
            'width' => 250,
            'flex-width' => true,
            'flex-height' => true,
        ));
    }
endif;
add_action('after_setup_theme', 'modern_art_successive_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function modern_art_successive_content_width()
{
    // This variable is intended to be overruled from themes.
    // Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
    // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
    $GLOBALS['content_width'] = apply_filters('modern_art_successive_content_width', 640);
}
add_action('after_setup_theme', 'modern_art_successive_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function modern_art_successive_widgets_init()
{
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'modern-art-successive'),
        'id' => 'sidebar-1',
        'description' => esc_html__('Add widgets here.', 'modern-art-successive'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}
add_action('widgets_init', 'modern_art_successive_widgets_init');

/**
 * Enqueue scripts and styles.
 */
function modern_art_successive_scripts()
{
    wp_enqueue_style('modern-art-successive-style', get_stylesheet_uri());

    wp_enqueue_script('modern-art-successive-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true);

    wp_enqueue_script('modern-art-successive-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}
add_action('wp_enqueue_scripts', 'modern_art_successive_scripts');

/**
 * Implement the Custom Header feature.
 */
require_once get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require_once get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require_once get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require_once get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
    require_once get_template_directory() . '/inc/jetpack.php';
}

// DB migration
require_once get_template_directory() . '/lib/db/migration.php';
// Classes 
require_once get_template_directory() . '/lib/classes/utils.php';
require_once get_template_directory() . '/lib/classes/users.php';
require_once get_template_directory() . '/lib/classes/arvPills.php';
require_once get_template_directory() . '/lib/classes/adherence.php';
require_once get_template_directory() . '/lib/classes/viralLoad.php';
require_once get_template_directory() . '/lib/classes/cd4Count.php';
require_once get_template_directory() . '/lib/classes/cf7.php';
require_once get_template_directory() . '/lib/classes/googleChart.php';
require_once get_template_directory() . '/lib/classes/notification.php';
require_once get_template_directory() . '/lib/classes/inbox.php';
// API
require_once get_template_directory() . '/lib/api/user.php';
require_once get_template_directory() . '/lib/api/dashboard.php';
require_once get_template_directory() . '/lib/api/cf7-api.php';
require_once get_template_directory() . '/lib/api/adherence.php';
require_once get_template_directory() . '/lib/api/arvPills.php';
require_once get_template_directory() . '/lib/api/viralLoad.php';
require_once get_template_directory() . '/lib/api/cd4Count.php';
require_once get_template_directory() . '/lib/registration-fields.php';
require_once get_template_directory() . '/lib/api/learn-more.php';
require_once get_template_directory() . '/lib/api/inbox.php';
require_once get_template_directory() . '/lib/api/notification.php';


require_once get_template_directory() . '/lib/cron.php';

// Custom Code for Theme - Daraptoor
add_action('after_setup_theme', 'register_my_menu');
function register_my_menu()
{
    register_nav_menu('secondary', __('Page Menu', 'page-menu'));
}
add_action('after_setup_theme', 'register_myLogout_menu');
function register_myLogout_menu()
{
    register_nav_menu('third', __('Logout Menu', 'logout-menu'));
}
add_action('after_setup_theme', 'register_mobile_menu');
function register_mobile_menu()
{
    register_nav_menu('forth', __('Mobile Menu', 'mobile-menu'));
}
function get_feature_image_url($postId, $size)
{
    $src = wp_get_attachment_image_src(get_post_thumbnail_id($postId), $size);
    return $src[0];
}

add_action('wp_login_failed', 'sTech_login_fail'); // hook failed login
function sTech_login_fail($username)
{
    $referrer = $_SERVER['HTTP_REFERER']; // where did the post submission come from?
    // if there's a valid referrer, and it's not the default log-in screen
    if (!empty($referrer) && !strstr($referrer, 'wp-login') && !strstr($referrer, 'wp-admin')) {
        wp_redirect(home_url() . '/login/?login=failed'); // let's append some information (login=failed) to the URL for the theme to use
        exit;
    }
}

add_action('wp_logout', 'ps_redirect_after_logout');
function ps_redirect_after_logout()
{
    wp_redirect('login');
    exit();
}

add_action('after_setup_theme', 'remove_admin_bar');
 
function remove_admin_bar() {
if (!current_user_can('administrator') && !is_admin()) {
  show_admin_bar(false);
}
}

function replace_core_jquery_version() {
    wp_deregister_script( 'jquery' );
    // Change the URL if you want to load a local copy of jQuery from your own server.
    wp_register_script( 'jquery', "https://code.jquery.com/jquery-3.1.1.min.js", array(), '3.1.1' );
}
add_action( 'wp_enqueue_scripts', 'replace_core_jquery_version' );

remove_action( 'register_new_user', 'wp_send_new_user_notifications' );


//custom page
